<?php
use Roots\Sage\Extras;

$image_full = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium', false);
$svg_img = get_field('svg_image');
$image = (!empty($image_full[0]))? ' style="background-image:url('.$image_full[0].')"' : ' style="background-image:url('.$svg_img['url'].')"';

$bg_class = (!empty($svg_img['url']))? 'bg-contain' : 'bg-cover';
?>
<div class="brands-item">
  <a href="<?php the_permalink(); ?>" class="item <?= $bg_class; ?>"<?= $image;?>>
    <h3 class="title"><?php the_title(); ?></h3>
    <div class="wrap-hover">
      <?php echo Extras\include_svg('kiradonjewel-logo-diamond'); ?>
      <div class="view-lookbook">View Lookbook</div>
    </div>
  </a>
</div>
