<?php
use Roots\Sage\Extras;

//MAILCHIMP
get_template_part('templates/mailchimp');

//ACF
$copyrights = get_field('copyrights','option');
$logo_diamond = get_field('diamond_logo','option');
$payments = get_field('payments_image','option');
?>

<footer class="footer-info" id="main-footer">
  <?php
  //links
    if( have_rows('footer_repeater','option') ):
      $i = 1;
      while ( have_rows('footer_repeater','option') ) : the_row();
        $title = get_sub_field('title');
        $counter = $i++;
        ?>
        <div class="col col-<?= $counter; ?>">
          <h5 class="mobile-toggle"><?= $title; ?></h5>
          <nav class="wrap">
            <?php the_sub_field('text_editor'); ?>
          </nav>
        </div>
        <?php
      endwhile;
    endif;
    ?>
</footer>

<?php
//FOLLOW US
get_template_part('templates/follow-us');
?>

<footer class="footer-bottom clearfix">

  <address class="copyrights">
    <?php
    if(!empty($logo_diamond)):
    ?>
    <a class="footer-diamond-logo" href="<?= esc_url(home_url('/')); ?>">
      <?php echo Extras\include_svg('kiradonjewel-logo-diamond'); ?>
    </a><?php endif; ?> ©<?php echo date("Y").' '.$copyrights; ?>
  </address>

  <div class="secure-payments" style="background-image:url(<?= $payments['url']; ?>);"></div>
</footer>
