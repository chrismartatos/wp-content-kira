
<?php

if( have_rows('fx_page_header') ):

    while ( have_rows('fx_page_header') ) : the_row();

      get_template_part('templates/flexible-content/fx-header-rows');

    endwhile;

  else :
    // no flexible content

endif;

?>
