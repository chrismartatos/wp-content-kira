<?php

/* Header
------------------------------------------------*/
if( get_row_layout() == 'header_slideshow' ):  get_template_part('templates/flexible-content/page-header/header_slideshow');

elseif( get_row_layout() == 'fullscreen_video' ): get_template_part('templates/flexible-content/page-header/header_video');

elseif( get_row_layout() == 'page_header' ): get_template_part('templates/flexible-content/page-header/page_header');


endif;

?>
