<?php
$title = get_sub_field('title');
$desc = get_sub_field('message');
$link = get_sub_field('link');
?>

<div class="page-header-container"<?php if(!empty($link)): ?> onclick="location.href='<?= $link; ?>';"<?php endif; ?>>
  <?php if(!empty($title)): ?>
  <section class="header-wrap-desc">
    <?php if(!empty($title)): ?>
    <h2 class="title"><?= $title; ?></h2>
    <?php endif; ?>

    <?php if(!empty($desc)): ?>
    <div class="header-editor-desc">
      <?= $desc; ?>
    </div>
    <?php endif; ?>
  </section>
  <?php endif; ?>
</div>
