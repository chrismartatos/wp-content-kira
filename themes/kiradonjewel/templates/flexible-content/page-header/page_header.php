<?php
  $img_header = get_sub_field('image');
  $bg_img = (!empty($img_header['url']))? ' style="background-image:url('.$img_header['url'].');"' : '' ;
?>
<div class="page-header bg-cover x-bg-fixed rellax-wrapper"<?= $bg_img; ?>>
  <?php get_template_part('templates/flexible-content/page-header/inner-header'); ?>
</div>
