<?php
global $wp_query;

  use Roots\Sage\Titles;

  $cat = $wp_query->get_queried_object();
  $icon = false;

  if($cat){
    $thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true );
    $icon = wp_get_attachment_url( $thumbnail_id );
  }

  $title = get_sub_field('title');
  $desc = get_sub_field('message');
  $active_desc = (!empty($header_desc))? ' desc-active' : '';
  ?>

  <div class="inner-page-header">
    <div class="x-rellax-init">
      <section class="container">
        <?php if(!empty($title)): ?>
        <h1 class="page-title"><?= $title;  ?></h1>
        <?php endif; ?>

        <?php if($icon): ?>
        <div class="icon-page-header bg-cover" style="background-image:url('<?= $icon;  ?>');"></div>
        <?php endif; ?>

        <?php if(!empty($desc)): ?>
        <article class="desc"><?= $desc; ?></article>
        <?php endif; ?>
      </section>
    </div>
  </div><!--END .inner-->
