<?php
use Roots\Sage\Titles;

$bg_video = get_sub_field('video_file');
$poster_img = get_sub_field('video_poster');

//Fields
$preloader_img = (!empty($post_img['url']))? ' poster="'.$poster_img['url'].'"': '' ;

if(!empty($bg_video)):
?>
<div class="fx-page-header page-header-bg-video" data-title="<?= Titles\title(); ?>">
    <video id="page-header-bg-video" autoplay playsinline muted loop autostart="true" preload="auto"<?= $preloader_img; ?>>
      <source src="<?= $bg_video['url']; ?>" type="video/mp4">
    </video>
    <?php get_template_part('templates/flexible-content/page-header/inner-header-box'); ?>
  </div>
</div>
<?php endif; ?>
