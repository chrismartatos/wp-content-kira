<div class="rellax-wrapper">
  <div class="rellax-init" data-rellax-speed="-3" data-rellax-percentage="0.5">

    <div id="slideshow-hero" class="header-slideshow">

      <!-- loader -->
      <span class="css-loader"></span>

      <?php
      //Repeater Field
      if( have_rows('slideshow') ):

        while ( have_rows('slideshow') ) : the_row();

        $bg_img = get_sub_field('image');
        $image = (!empty($bg_img['url']))? ' style="background-image:url('.$bg_img['url'].')"' : '';
        ?>

        <?php if($bg_img): ?>
        <article class="slide-item bg-cover"<?= $image; ?>>
          <?php get_template_part('templates/flexible-content/page-header/inner-header-box'); ?>
        </article>
        <?php endif; ?>

        <?php endwhile; ?>
      <?php endif; ?>
    </div><!--END #slideshow-->

  </div>
</div>
