<?php

if( have_rows('fx_add_content') ):

  while ( have_rows('fx_add_content') ) : the_row();

  get_template_part('templates/flexible-content/fx-layouts');

  endwhile;

else :

// No rows

endif;

?>
