<?php
/* New Custom Section
------------------------------------------------------------*/
if( get_row_layout() == 'text_editor' ): get_template_part('templates/flexible-content/elements/wysiwyg');

/* Add fx sections - whatever
------------------------------------------------------------*/
elseif( get_row_layout() == 'intro_text' ): get_template_part('templates/flexible-content/elements/intro_text');

elseif( get_row_layout() == 'featured_items' ): get_template_part('templates/flexible-content/elements/featured_items');

elseif( get_row_layout() == 'featured_products' ): get_template_part('templates/flexible-content/elements/featured_products');

elseif( get_row_layout() == 'featured_products' ): get_template_part('templates/flexible-content/elements/featured_products');

elseif( get_row_layout() == 'lookbook_gallery' ): get_template_part('templates/flexible-content/elements/lookbook_gallery');

elseif( get_row_layout() == 'show_lookbooks' ): get_template_part('templates/flexible-content/elements/lookbook');

elseif( get_row_layout() == 'instagram_feed' ): get_template_part('templates/flexible-content/elements/instagram_feed');


endif;

?>
