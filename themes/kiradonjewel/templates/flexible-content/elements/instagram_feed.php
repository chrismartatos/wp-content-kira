
<div id="wrap-insta-feed" class="cf">
	<div id="instagram-feed" class="clearfix" data-hashtag="" data-count="">
		<button class="sbcnt-inst-prev fa fa-left-chevron"></button>
		<button class="sbcnt-inst-next fa fa-right-chevron"></button>
	</div>
</div>

<script type="template" id="instagram-template">
	<figure class="sbcnt-inst-item">
		<figcaption>
			<a class="sbcnt-inst-link info" target="_blank">
				<div class="vcenter-outer">
					<div class="vcenter-inner">
						<span class="sbcnt-inst-caption"></span>
						<span class="sbcnt-inst-handle"></span>
					</div>
				</div>
			</a>
		</figcaption>
	</figure>
</script>
