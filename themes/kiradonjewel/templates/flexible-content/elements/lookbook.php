<?php
/*
Template Name: Lookbook
*/
?>

<?php
  //The content
  get_template_part('templates/the-content');


$lookbook_args = array(
  'post_type' => 'lookbook',
  'post_status' => 'publish',
  'orderby' => 'date',
  'post_per_page' => -1,
  'nopaging' => true
);

//Query
$lookbook_query = new WP_Query( $lookbook_args );
?>

<div class="clearfix brands-grid visible-el">
 <?php
 if($lookbook_query->have_posts()) :
   while ($lookbook_query->have_posts()) : $lookbook_query->the_post();
      get_template_part('templates/lookbook/entry-post');
   endwhile;

   wp_reset_postdata();
 endif;
 ?>
</div>
