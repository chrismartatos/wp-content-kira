<div class="intro-text visible-el">
  <div class="wrap">
    <section class="container">
      <?php the_sub_field('text'); ?>
    </section>
  </div>
</div>
