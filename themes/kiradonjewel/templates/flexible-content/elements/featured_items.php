<?php

use Roots\Sage\Media;
use Roots\Sage\Extras;
?>

<?php if (have_rows('featured')): ?>

<div class="services-wrap visible-el">

  <?php while (have_rows('featured')): the_row();

    $title = get_sub_field('title');
    $url   = get_sub_field('page');
    $info  = get_sub_field('description');
    $image_land  = get_sub_field('image_landscape');
    $image_port  = get_sub_field('image_portrait');
    $icon   = get_sub_field('icon');

    $term_url = (!empty($url))?get_term_link($url,'product_cat'):'#';

    $image_bg = (wp_is_mobile())?$image_port['url']:$image_port['url'];
  ?>

    <div class="service">
      <a href="<?= $term_url; ?>">

        <div class="service-bg-img" style="background-image:url(<?= $image_bg; ?>);"></div>

        <header class="service-title">
          <h4 class="title"><?= $title; ?></h4>
        </header>

        <?php if(!empty($info)):?>
        <div class="service-banner">
          <div class="service-banner-icon">
            <?php if(!empty($icon['url'])): ?>
            <div class="svg-container arrow-svg-wrap">
              <img src="<?= $icon['url']; ?>" alt="<?= $title; ?>">
            </div>
            <?php endif; ?>
          </div>

          <article class="service-banner-text">
            <?= $info; ?>
          </article>
        </div>
        <?php endif; ?>

      </a>
    </div>

  <?php endwhile; ?>

</div>

<?php endif; ?>
