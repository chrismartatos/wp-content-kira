<div class="lookbook-gallery-wrapper clearfix visible-el">
<?php if( have_rows('add_gallery') ): ?>
 	<?php
  $counter = 1;
  $arrowprev = 1;
  $arrownext = 1;
  ?>

  <?php while( have_rows('add_gallery') ): the_row(); ?>
    <div class="brand-alpha gallery-<?= $counter++; ?>">

      <?php
      $images = get_sub_field('gallery');
      $speed = get_sub_field('speed');
      $file = get_sub_field('file');
      ?>

      <?php if( $images ):   ?>

        <div class="brand-slideshow fullscreen-image absolute-arrows" data-speed="<?= $speed; ?>">
          <a class="arrow prev prev-<?= $arrowprev++; ?>"></a>
          <a class="arrow next next-<?= $arrownext++; ?>"></a>
          <?php foreach( $images as $image ): ?>

          <?php
          $product = get_field('choose_product', $image['ID']);
          ?>
          <div class="slide-item">
            <img data-lazy="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>">
            <?php
            if(!empty($product)):
              foreach( $product as $acf_object)
              {
                $wc_product = wc_get_product( $acf_object->ID );
                if($wc_product)
                {
                  echo '<div class="lookbook-product-info">
                    <a href="'.$wc_product->get_permalink().'">
                      <h4 class="title">'.$wc_product->get_title().'</h4>
                      <span class="price">'.woocommerce_price($wc_product->get_price()).'</span>
                      <span class="buy-now">&larr; View</span>
                    </a>
                  </div>';
                }
              }
            endif;
            ?>
          </div>
          <?php endforeach; ?>
        </div>
        <?php endif; ?>
    </div>
  <?php endwhile; ?>
<?php endif; ?>
</div>
