<?php

use Roots\Sage\Extras;


$title       = get_sub_field('title');
$subtitle    = get_sub_field('subtitle');
$image       = get_sub_field('image');
$content     = get_sub_field('content');

$button_text = get_sub_field('button_text');
$button_url  = get_sub_field('button_link');

$img_src     = wp_get_attachment_image_url( $image['id'], 'full' );
$img_srcset  = wp_get_attachment_image_srcset( $image['id'], 'full' );

$dna_bg     = get_sub_field('add_dna_background');
$bg_color     = get_sub_field('background_color');
$width   = get_sub_field('columns_width');

$id   = get_sub_field('id');
?>

<div class="picture-card-section bt-expand std-mtb scroll-section visible-el" data-id="<?php echo $id; ?>">

  <div class="picture-card add-dna-<?php echo $dna_bg; ?> color-<?php echo $bg_color; ?> width-<?php echo $width; ?>">

    <div class="picture-card-img-wrap">
      <img src="<?= esc_url( $img_src ); ?>" srcset="<?= esc_attr( $img_srcset ); ?>" alt="<?= $image['alt']; ?>">
    </div>

    <div class="picture-card-content-wrap">

      <div class="bt-container">
        <div class="bt-row">
          <div class="col-xs-12">

            <div class="picture-card-content-inner">

              <hgroup class="card-title">
                <h3 class="subtitle"><?= ($title) ? $title : ''; ?></h3>
                <h4 class="title"><?= ($subtitle) ? $subtitle : ''; ?></h4>
              </hgroup>
              <div class="border-gradient"></div>
              <div class="entry-content"><?= ($content) ? $content : ''; ?></div>

              <div class="picture-card-cta cta-btn-wrap">
                <?php if(!empty($button_url)):?>
                <a class="button" href="<?= ($button_url) ? $button_url : ''; ?>">
                  <?= ($button_text) ? $button_text : ''; ?>
                </a>
              <?php endif; ?>
              </div>

            </div>

          </div>
        </div>
      </div>

    </div><!-- end of picture-card-content-wrap -->

  </div><!-- end of picture-card -->


</div><!-- end of picture-card-section -->
