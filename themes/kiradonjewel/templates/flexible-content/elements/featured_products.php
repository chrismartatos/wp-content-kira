<?php

use Roots\Sage\Extras;
use Roots\Sage\Titles;


$raw_section_title = get_sub_field('title');
$subtitle = get_sub_field('subtitle');
$bg_img    = get_sub_field('background_image');
$btn_text  = get_sub_field('button_title');
$cat       = (array) get_sub_field('choose_category');

if(!empty($cat)):

$products_args = array(
  'post_type' => 'product',
  'posts_per_page' => '8',
  'tax_query' => array (
    array (
      'taxonomy' => 'product_cat',
      'field' => 'term_id',
      'terms' => array($cat['term_id'])
    ),
  ),
  'orderby' => 'date',
  'ignore_sticky_posts' => 1
);

$posts_query = new WP_Query( $products_args );
?>

<div class="projects-wrap bg-cover x-bg-fixed visible-el" style="background-image: url(<?= $bg_img['url'];?>);">
  <div class="container">
    <div class="division-title">
      <span class="white-text"><?= $subtitle; ?></span>
    </div>
    <div class="projects-title">
      <?= $raw_section_title; ?>
    </div>

    <div class="project-slideshow-wrap">
      <div class="project-slideshow slick-slideshow" id="project-slideshow">

        <a href="#" class="project-slide-arrow project-slide-arrow-left fa fa-chevron-left"></a>
        <a href="#" class="project-slide-arrow project-slide-arrow-right fa fa-chevron-right"></a>

        <?php while ( $posts_query->have_posts() ): $posts_query->the_post(); ?>

          <a class="product-<?= $post->ID; ?> slide-item" href="<?php echo get_permalink($post->ID); ?>">

            <div class="project-slide-item-wrap">
              <div class="project-slide-item">
                <?php
                $product = wc_get_product( $post->ID );
                $attachment_ids = $product->get_gallery_attachment_ids();
                $class = (!empty($attachment_ids[0]))? 'hover-image': 'hover-empty';
                ?>
                <div class="project-slide-item-img">
                  <?php
                  echo '<div class="clearfix product-thumb-hover-wrap '.$class.'" data-id="'.$attachment_ids[0].'">';
                  echo wp_get_attachment_image($attachment_ids[0],'shop_catalog',false,'data-hover');
                  echo woocommerce_get_product_thumbnail();
                  echo '</div>';
                  ?>
                </div>

                <div class="project-slide-item-title-wrap">
                  <h5 class="project-slide-item-title">
                    <?= $product->get_title(); ?>
                  </h5>
                  <div class="price"><?= woocommerce_price($product->get_price()); ?></div>
                </div><!-- end of project-title-wrap -->

              </div><!-- end of project-slide-item -->
            </div><!-- end of project-slide-item-wrap -->

          </a>

        <?php endwhile; wp_reset_postdata();?>
      </div><!-- end of project-slideshow -->


    </div><!-- end of project-slideshow-wrap -->

    <div class="project-slideshow-cta">
      <div class="cta-button">
        <a class="btn button white-button"  href="<?php echo get_term_link($cat['term_id'],'product_cat'); ?>">
          <?= $btn_text; ?>
        </a>
      </div>
    </div>

  </div>
</div>
<?php endif; ?>
