<?php if( ! post_password_required() ): ?>

    <?php get_template_part('templates/flexible-content/fx-content'); ?>

<?php elseif( post_password_required() ): ?>
  <!-- PASSWORD PROTECTED AREA --->
  <div class="container password-protected">
    <div class="row">
      <div class="col-xs-12 col-md-4 offset-md-4">
        <?php the_content(); ?>
      </div>
    </div>
  </div>
  <?php endif; ?>
