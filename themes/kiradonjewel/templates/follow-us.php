<nav class="social-icons clearfix">
	<h5>Follow us: </h5>
<?php if( have_rows('add_social_media', 'options') ): ?>

	<ul>

	<?php while( have_rows('add_social_media', 'options') ): the_row();

		// vars
		$title = get_sub_field('title');
		$url   = get_sub_field('url');
		$icon  = get_sub_field('icon');
    $open_in_a_new_window = get_sub_field('open_on_a_new_window');
	?>

		<?php if( $url ): ?>

			<li>
				<a href="<?= $url; ?>" class="icon social-follow-<?= $icon;?>"<?php if($open_in_a_new_window=='Yes'):?> target="_blank" rel="noopener"<?php endif;?> title="<?= $title;?>">
          <i class="fa fa-<?= $icon;?>" aria-hidden="true"></i>
        </a>
			</li>

		<?php endif; ?>

	<?php endwhile; ?>

	</ul>

<?php endif; ?>

</nav>
