<?php
$logo_maythejewel = get_field('maythejewel_logo','option');
$side_notification = get_field('mailchimp_notification','option');
$title = the_field('mailchimp_title', 'option');
?>

<?php if(!empty($side_notification)): ?>
<nav class="side-notification">
  <a href="#mailchimp-footer-form" class="scroll-to" data-offset="-230"><?= $side_notification; ?></a>
</nav>
<?php endif; ?>

<section id="mailchimp-footer-form" class="footer-newsletter-form">
  <?php
  if(!empty($logo_maythejewel)):
  ?>
  <div class="icon-maythejewel">
    <img width="280" src="<?= $logo_maythejewel['url']; ?>" alt="may the jewel be with you">
  </div>
  <?php endif; ?>

  <div class="wrap">
    <?php if(!empty($title)): ?>
    <h4 class="title"><?php the_field('mailchimp_title', 'option'); ?></h4>
    <?php endif; ?>

    <div class="mailchimp-form-wrap">
      <?php the_field('mailchimp_form','option'); ?>
    </div>
  </div>

</section>
