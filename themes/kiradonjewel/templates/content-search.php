<?php
$image_url_full = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumbnail', false);
?>

<article <?php post_class('entry-content search-post'); ?>>
  <header>
    <h3 class="entry-title"><?php the_title(); ?></h3>
    <?php if (get_post_type() === 'post') { get_template_part('templates/entry-meta'); } ?>
  </header>
  <div class="entry-summary clearfix">
    <div class="col col-img">
      <?php
      if($image_url_full):
        $feat_img = $image_url_full[0];
      ?>
      <img class="alignleft" src="<?= $feat_img; ?>" width="150" alt="<?php the_title(); ?>">
      <?php endif; ?>
    </div>

    <div class="col">
      <?php the_excerpt(); ?>
    </div>
  </div>
  <a href="<?php the_permalink(); ?>" class="read-more">Read more</a>
</article>
