<?php
$image_url_full = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium', true);
$feat_img = $image_url_full[0];
?><a href="<?php the_permalink(); ?>" <?php post_class('ajax-post'); ?>>
  <img src="<?= $feat_img; ?>" alt="<?php the_title(); ?>">
  <header>
    <h2 class="entry-title"><?php the_title(); ?></h2>
  </header><span class="read-more">Read more +</span></a>
