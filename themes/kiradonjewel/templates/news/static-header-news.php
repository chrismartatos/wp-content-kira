<?php
  use Roots\Sage\Titles;

  $img_header = get_field('image_header',get_option('page_for_posts'));
  $bg_header = $img_header['url'];
  $bg_img = (!empty($bg_header))? ' style="background-image:url('.$bg_header.');"' : '' ;

  $hide_page_title = get_field('hide_page_title',get_option('page_for_posts'));
  $page_title = Titles\title();
  $header_title = get_field('header_title',get_option('page_for_posts'));
  $main_title = (!empty($header_title))? $header_title : $page_title;

  $header_desc = get_field('header_description',get_option('page_for_posts'));
  $active_desc = (!empty($header_desc))? ' desc-active' : '';
?>
<div class="page-header bg-cover bg-fixed"<?= $bg_img; ?>>

  <?php if($header_desc): ?>
  <div class="wrap-desc">
  <?php endif; ?>

  <div class="vcenter-outer">
    <div class="vcenter-inner">

      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-2"></div>

          <div class="col-sm-12 col-md-8">
            <?php if($hide_page_title !== "Yes"): ?>
            <h1 class="page-title<?= $active_desc; ?>"><?= $main_title;  ?></h1>
            <?php endif; ?>

            <?php if($header_desc): ?>
            <div class="desc"><p><?= $header_desc; ?></p></div>
            <?php endif; ?>
          </div>

          <div class="col-sm-12 col-md-2"></div>
        </div>
      </div><!--END .container-->

    </div><!--END .vcenter-inner-->
  </div><!--END .vcenter-outer-->

  <?php if($header_desc): ?>
  </div><!--END .wrap-desc-->
  <?php endif; ?>
</div>
