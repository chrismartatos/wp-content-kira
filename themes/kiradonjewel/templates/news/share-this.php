<?php
$uri = ''; //get_home_url();
$path = get_the_permalink();
$title = get_the_title();
?>

<nav class="share-buttons">
  <a href="#" class="anchor-hover">Share this <span class="plus">+</span></a>
  <ul class="share-social">
    <li><a target="_blank" href="http://www.facebook.com/share.php?u=<?= $uri.$path; ?>&amp;t=<?= $title; ?>" class="fa fa-facebook social-fb" title="Share on Facebook"></a></li>
    <li><a target="_blank" href="http://twitter.com/share?text=<?php ?>&amp;url=<?= $uri.$path; ?>" class="fa fa-twitter social-tw" title="Share on Twitter"></a></li>
    <li><a target="_blank" href="mailto:example@email.com?subject=<?= $title; ?>&amp;body=<?= $uri.$path; ?>" class="fa fa-envelope social-em" title="Share via Email"></a></li>
  </ul>
</nav>
