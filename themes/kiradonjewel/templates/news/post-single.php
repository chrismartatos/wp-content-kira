<?php
while (have_posts()) : the_post();

$image_url_full = wp_get_attachment_image_src(get_post_thumbnail_id(), 'large', false);
$feat_img = $image_url_full[0];

$wrapper = (!empty($feat_img))? " col-md-8" : " col-md-12" ;
?>
<article <?php post_class(); ?>>
  <header>
    <h1 class="entry-title"><?php the_title(); ?></h1>
  </header>
  <div class="row">
    <?php
    get_template_part('templates/news/featured-image');
    ?>

    <div class="col-sm-12<?= $wrapper; ?>">
      <div class="post-wrapper">
        <article class="entry-content">
          <?php the_content(); ?>
        </article>
        <?php get_template_part('templates/news/share-this'); ?>
      </div>

    </div>
  </div>
</article>
<?php endwhile; ?>
