<?php
$image_url_full = wp_get_attachment_image_src(get_post_thumbnail_id(), 'large', false);
$feat_img = $image_url_full[0];
?>

<?php if(!empty($feat_img)): ?>
<div class="col-sm-12 col-md-4">
  <div class="img-wrap">
    <img src="<?= $feat_img; ?>" alt="<?php the_title(); ?>">
  </div>
</div>
<?php endif; ?>
