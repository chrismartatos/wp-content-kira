<?php
while (have_posts()) : the_post();
  woocommerce_breadcrumb();
  the_content();
endwhile;
?>
