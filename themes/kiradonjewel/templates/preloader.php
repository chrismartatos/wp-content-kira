<?php
use Roots\Sage\Extras;

$logo_diamond = get_field('diamond_logo','option');

if(!empty($logo_diamond)):
?>

<script type="text/javascript">

  //plain js - loader script
  var js_target_preloader = document.getElementById("preloader");
  var js_taget_diamond = document.getElementById("preloader-diamond");
  var js_svg_diamond = document.getElementById("diamond-svg");
  var js_target_body = document.getElementsByTagName("BODY")[0];
  var _delay = 0;
  var _css_transition = 2000;
  var _css_transition_end = 2600;

  //Storage
  if (typeof(Storage) !== "undefined")
  {
    trigger_preloader_diamond();
  } else {
    trigger_preloader_kiralogo();
  }

  //Function
  function trigger_preloader_diamond()
  {
    if(sessionStorage.getItem('kiradonjewel_loader') !== 'muerto')
    {
      //Start session - first session
      sessionStorage.setItem('kiradonjewel_loader', 'muerto');

      //loading diamond
      setTimeout(function(){js_target_body.classList.add('diamond-loading'); },_delay);

      //loading animation
      setTimeout(function(){js_target_body.classList.add('end-transition'); },_css_transition);

      //End
      setTimeout(function()
      {
        js_target_body.classList.add('diamond-loaded');
        js_target_body.classList.add('loaded');
      },_css_transition_end);
    } else {
      trigger_preloader_kiralogo();
    }
  }

  //Function
  function trigger_preloader_kiralogo()
  {
    js_target_body.classList.add('loading');

    $(window).load(function()
    {
      js_target_body.classList.add('loaded');
    });
  }
</script>

<div id="preloader" class="main-preloader">
  <div id="preloader-diamomd" class="preloader-diamond">
    <?php echo Extras\include_svg('kiradonjewel-logo-diamond'); ?>
  </div>
  <div class="loading-text">Loading</div>
</div>
<?php endif; ?>
