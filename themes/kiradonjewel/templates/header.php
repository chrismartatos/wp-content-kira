<?php
use Roots\Sage\Extras;

$logo = get_field('website_logo','option');
$logo_diamond = get_field('diamond_logo','option');
?>
<div id="main-header" class="main-header-wrapper">

  <div id="header-search-form" class="wrap-search-form">
    <?php get_product_search_form(); ?>
  </div>

  <header class="header-top clearfix" id="main-header-top">

    <div class="top-nav-left">
      <?php
      if (has_nav_menu('header_top_navigation')) :
        wp_nav_menu(['theme_location' => 'header_top_navigation', 'menu_class' => 'header-nav-left']);
      endif;
      ?>
    </div>

    <a class="top-diamond-logo" href="<?= esc_url(home_url('/')); ?>">
      <?php echo Extras\include_svg('kiradonjewel-logo-diamond'); ?>
    </a>

    <div class="top-nav-right">
      <ul class="user-nav">
        <li id="header-account-btn" class="icon-link">
          <a class="icon icon-account" href="#" title="Account">Account</a>
          <?php
          if (has_nav_menu('account_navigation')) :
            wp_nav_menu(['theme_location' => 'account_navigation', 'menu_class' => 'sub-menu account-nav']);
          endif;
          ?>
        </li>
        <li class="icon-link">
          <?php $wishlist_counter = yith_wcwl_count_all_products(); ?>

          <a id="header-wishlist-btn" class="icon icon-wishlist" href="<?= get_permalink(get_page_by_path('my-wishlist')); ?>" title="Wishlist"><?php if($wishlist_counter>0){ echo '<span>'.$wishlist_counter.'</span>'; } ?></a>
        </li>
        <li class="icon-link">
          <a id="header-search-btn" class="icon icon-search" href="#" title="Search">Search</a>
        </li>
        <li class="icon-link">
          <a<?php if(is_cart() || is_checkout()){ /*do nothing*/} else { echo ' id="header-cart-btn"'; } ?> class="header-cart-btn icon icon-cart" href="<?php echo wc_get_cart_url(); ?>" title="Cart">
            <span id="ajax-cart-counter" class="account-cart-counter"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
          </a>
        </li>
      </ul>
    </div>
  </header>
  <header class="head-main-logo" id="main-header-middle">
    <a class="main-logo" href="<?= esc_url(home_url('/')); ?>">
      <img src="<?= $logo['url']; ?>" alt="<?php bloginfo('name'); ?>">
    </a>

    <a id="hamburger" href="#">
      <span class="line-1 line"></span>
      <span class="line-2 line"></span>
    </a>
  </header>
  <header class="header-info" id="main-header-bottom">
    <nav id="header-nav-wrap" class="nav-primary header-nav-wrap">
      <?php
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'main-header-ul']);
      endif;
      ?>
    </nav>
  </header>
</div>
