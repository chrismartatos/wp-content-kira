
<?php
// get the current taxonomy term
$term = get_queried_object();

if( have_rows('cloned_page_header',$term)):

    while ( have_rows('cloned_page_header',$term) ) : the_row();

      get_template_part('templates/flexible-content/fx-header-rows');

    endwhile;

  else :
    // no flexible content

endif;

?>
