<?php
$user = wp_get_current_user();
$msg_customer = get_field('shop_notification','options');
$msg_wholesale = get_field('notification_wholesale_customers','options');

if(!empty($msg_customer) || !empty($msg_wholesale)):
?>
<div class="customer-notification hide msg-bottom">
  <?php
  if ( in_array( 'wholesale_customer', (array) $user->roles ) ): echo $msg_wholesale; else: echo $msg_customer; endif;
  ?>
</div>
<?php endif; ?>
