<div id="append-post" class="clearfix animate-post">
  <div class="append-post">
    <?php get_template_part('templates/news/post-single'); ?>
  </div>
</div>

<div class="posts-wrapper">
  <div class="paging-content">
    <?php
		$temp = $wp_query;
    $wp_query= null;
		$wp_query = new WP_Query();
    $wp_query->query('posts_per_page=12' . '&paged='.$paged);

		while ($wp_query->have_posts()) : $wp_query->the_post();
      get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format());
    endwhile;
    ?>
  </div>
</div>
<nav id="paging" class="clear">
  <div class="nav-previous">
    <?php
    $next = get_next_posts_link( 'Load More <span class="css-loader"></span>', $wp_query->max_num_pages );
    echo $next;
    ?>
  </div>
</nav>
</div>
<?php wp_reset_postdata(); ?>
