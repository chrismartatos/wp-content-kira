<?php
if(have_posts()):
  while (have_posts()) : the_post();
  $content = apply_filters( 'the_content', get_the_content() );
  if(!empty($content)):
  ?>
  <section class="page-main-content entry-content">
    <div class="entry-content-container container">
      <div class="row">
        <div class="col-sm-12 col-md-12">
          <article class="content-wrap ">
            <?php echo $content; ?>
          </article>
        </div>
      </div>
    </div>
  </section>
  <?php
  endif;
  endwhile;
endif;
?>
