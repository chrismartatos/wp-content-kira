/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function()
      {
        var $body = $('body');
        var $window = $(window);
        var $matchCard = $body.find('.match-height-js');
        var _windowH = $window.height()-200;

        //Reset -Fade Out Transition
        $body.removeClass('page-reload-transition');

        //Lookbook
        if($body.find('.brands-grid').length){ $body.addClass('lookbook-gallery'); }


        /************** FN: Page Transition *****************
        ----------------------------------------------------*/
        var $js_transition = $('#mobile-menu-wrap a, #main-footer .menu-item a, #main-header .main-logo, #main-header .menu-item:not(.menu-item-has-children) > a,.page-transition:not([href^="#"],[href*="wp-login"],[href*="wp-admin"])');

        $js_transition.each(function()
        {
          $(this).on('click', {},function(e)
          {
            //Transitions init - Fade out
            $body.addClass("page-reload-transition");
          });
        });


        /* FN: Old technique - min height for main content - remove later
        ----------------------------------------------------*/
        $body.find('.main-content[role="document"]').css('min-height',_windowH+'px');

        /* FN: HEADER - add .not('.home') to exclude
        ----------------------------------------------------*/
        $body.hideHeader_onScrollingDown();
        $('#main-header').headerScrollingFN();
        $('#hamburger').mobileNav();
        $('#header-search-btn').headerSearchIcon();
        $('#header-account-btn, #header-nav-wrap > ul:first > .menu-item-has-children').hoverIntentPlugin();

        /* FN: Slideshows - Home
        ----------------------------------------------------*/
        $('#slideshow-hero').hero_slideshow();
        $('#project-slideshow').productSlideshow();

        /* FN: Scroll To element
        ----------------------------------------------------*/
        $body.find('.scroll-to').scrollToElement();

        /* FN: Visible element
        ----------------------------------------------------*/
        $body.find('.visible-el').visibleElementFN();

        /* FN: Touched Bottom
        ----------------------------------------------------*/
        $body.touchBottomFN();

        /* FN: FAQ item
        ----------------------------------------------------*/
        $body.find('.faq-item').faqToggle();

        /* FN: Footer Toggle
        ----------------------------------------------------*/
        $('#main-footer .mobile-toggle').footerToggle();

        /* Checkout cart toggle
        ---------------------------------------------------------------*/
        $('#header-cart-btn').openMenuToggle();
        $body.find('.go-to-newsletter').openMenuToggle();

        /*  FN: Ajax Load More
        ------------------------------------------------------------------*/
        $("#paging-products").productsLoadMore();

        /*  FN: Ajax Load More
        ------------------------------------------------------------------*/
        $("#paging").ajaxLoadMore();

        /*  FN: Ajax post
        ------------------------------------------------------------------*/
        $body.find(".ajax-post:not(.active)").getAjaxPost();

        /*  FN: Share
        ------------------------------------------------------------------*/
        $body.find(".share-buttons .anchor-hover").shareThis();

        /*  FN: Brands
        ------------------------------------------------------------------*/
        $body.find(".brand-alpha").lookbookAlpha();

        /*  Parallax
        ------------------------------------------------------------------*/
        $window.load(function()
        {
          if($body.find('.rellax-init').length)
          {
            if($(window).width()>=767)
            {
              var rellax = new Rellax('.rellax-init');
            }
          }
        });
      },

      finalize: function()
      {
        // JavaScript to be fired on all pages, after page specific JS is fired

        /*******  Woocommerce: Mini Cart
        ========================================================*/
        $("#ajax-mini-cart-data").KIRADONJEWEL_miniCart();

        //Added to cart
        $( document.body ).on( 'added_to_cart', function()
        {
          $(window).MiniCart_AjaxRequest();

          setTimeout(function()
          {
            $('#mini-cart').addClass('menu-open');
            $('.site-container-cart').addClass('menu-open');

            //Css animation
            $('body').addClass('product-added').addClass('mini-cart-opened');
          },900);
        });


        /*******  Hide email - .hide-email, hide-mailto
        ========================================================*/
        $('.hide-email').hideEmail();
        $('.hide-mailto').hideMailTo();

        /*******  Added to cart
        ========================================================*/
        var $woo_notification = $('.woocommerce-notices-wrapper');

        if($woo_notification.length)
        {
          var $woo_msg = $woo_notification.find('.woocommerce-message[role="alert"]').text();

          if($woo_msg.includes("added"))
          {
            $(window).load(function()
            {
              setTimeout(function(){
                $('#header-cart-btn').click();
              },1000);
            });
          }
        }//end added to cart

      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page

      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);



})(jQuery); // Fully reference jQuery after this point.


$.fn.hideEmail = function()
{
  var $target = $(this);

  if($target.length)
	{
		$target.each(function()
		{
			if($(this).attr('data-show')==="true")
			{
				var $_1 = $(this).attr('data-name');
				var $_2 = $(this).attr('data-domain');

				$(this).attr('href','mailto:'+$_1+'@'+$_2).text($_1+'@'+$_2);
			} else {
				$(this).attr('href','mailto:'+$(this).attr('data-name')+'@'+$(this).attr('data-domain'));
			}
		});
	}
};

$.fn.hideMailTo = function()
{
  var $target = $(this);

  if($target.length)
	{
		$target.find('a').each(function()
		{
      var $href = $(this).attr('href');
      var _str = $href.split('mailto:');
      var _get = _str[1];

      $(this).attr('href','mailto:'+_get.split('-')[0]+'@'+_get.split('-')[1]);

      if($(this).attr('data-show')){
        $(this).text(_get.split('-')[0]+'@'+_get.split('-')[1]);
      }
		});
	}
};


/*------------------------------------------------------------------------------------
    Mobile
-------------------------------------------------------------------------------------*/
$.fn.mobileNav = function()
{
  var $target = $(this);

  if($target.length)
  {
    $main_nav = $('#header-nav-wrap');

		$target.click(function(e)
		{
			e.preventDefault();

			if( !$target.hasClass("active") )
      {
        $main_nav.slideDown();
        $target.addClass("active");
			} else {
        $main_nav.slideUp();
        $target.removeClass("active");
      }

			return false;
		});
  }
};

/*------------------------------------------------------------------------------------
    Brand Alpha
-------------------------------------------------------------------------------------*/

$.fn.lookbookAlpha = function()
{
  var $target = $(this);

  if($target.length)
  {
    var _default_speed = 4000;
    var $gallery_one   = $(".gallery-1").find(".brand-slideshow");
    var $gallery_two   = $(".gallery-2").find(".brand-slideshow");
    var $gallery_three = $(".gallery-3").find(".brand-slideshow");
    var $gallery_four = $(".gallery-4").find(".brand-slideshow");
    var $gallery_five = $(".gallery-5").find(".brand-slideshow");
    var $gallery_six = $(".gallery-6").find(".brand-slideshow");
    var $gallery_7 = $(".gallery-7").find(".brand-slideshow");
    var $gallery_8 = $(".gallery-8").find(".brand-slideshow");
    var $gallery_9 = $(".gallery-9").find(".brand-slideshow");
    var $gallery_10 = $(".gallery-10").find(".brand-slideshow");
    var $gallery_11 = $(".gallery-11").find(".brand-slideshow");
    var $gallery_12 = $(".gallery-12").find(".brand-slideshow");

    if($gallery_one.length)
    {
      var _speed1 = ($gallery_one.attr("data-speed").length)?$gallery_one.attr("data-speed"):_default_speed;

      $gallery_one.slick({
    		autoplay:true,
    		slide:".slide-item",
    		autoplaySpeed:parseInt(_speed1),
        speed:1000,
    		arrows:false,
    		dots:true,
        fade:true,
        rows: 0,
    		infinite:true,
    		slidesToShow:1,
    		slidesToScroll:1,
        lazyload:'ondemand',
        prevArrow:$(".prev-1"),
        nextArrow:$(".next-1"),
        customPaging : function(slider, i) {return '<span class="dot"></span>';}
  		});
    }

    if($gallery_two.length)
    {
      var _speed2 = ($gallery_two.attr("data-speed").length)?$gallery_two.attr("data-speed"):_default_speed;

      $gallery_two.slick({
    		autoplay:true,
    		slide:".slide-item",
    		autoplaySpeed:parseInt(_speed2),
        speed:1000,
    		arrows:false,
    		dots:true,
        fade:true,
        rows: 0,
    		infinite:true,
    		slidesToShow:1,
    		slidesToScroll:1,
        lazyload:'ondemand',
        prevArrow:$(".prev-2"),
        nextArrow:$(".next-2"),
        customPaging : function(slider, i) {return '<span class="dot"></span>';}
  		});
    }

    if($gallery_three.length)
    {
      var _speed3 = ($gallery_three.attr("data-speed").length)?$gallery_three.attr("data-speed"):_default_speed;

      $gallery_three.slick({
    		autoplay:true,
    		slide:".slide-item",
    		autoplaySpeed:parseInt(_speed3),
        speed:1000,
    		arrows:false,
    		dots:true,
        fade:true,
    		infinite:true,
        rows: 0,
    		slidesToShow:1,
    		slidesToScroll:1,
        lazyload:'ondemand',
        prevArrow:$(".prev-3"),
        nextArrow:$(".next-3"),
        customPaging : function(slider, i) {return '<span class="dot"></span>';}
  		});
    }

    if($gallery_four.length)
    {
      var _speed4 = ($gallery_four.attr("data-speed").length)?$gallery_four.attr("data-speed"):_default_speed;

      $gallery_four.slick({
    		autoplay:true,
    		slide:".slide-item",
    		autoplaySpeed:parseInt(_speed4),
        speed:1000,
    		arrows:false,
    		dots:true,
        fade:true,
        rows: 0,
    		infinite:true,
    		slidesToShow:1,
    		slidesToScroll:1,
        lazyload:'ondemand',
        prevArrow:$(".prev-4"),
        nextArrow:$(".next-4"),
        customPaging : function(slider, i) {return '<span class="dot"></span>';}
  		});
    }

    if($gallery_five.length)
    {
      var _speed5 = ($gallery_five.attr("data-speed").length)?$gallery_five.attr("data-speed"):_default_speed;

      $gallery_five.slick({
    		autoplay:true,
    		slide:".slide-item",
    		autoplaySpeed:parseInt(_speed5),
        speed:1000,
    		arrows:false,
    		dots:true,
        fade:true,
        rows: 0,
    		infinite:true,
    		slidesToShow:1,
    		slidesToScroll:1,
        lazyload:'ondemand',
        prevArrow:$(".prev-5"),
        nextArrow:$(".next-5"),
        customPaging : function(slider, i) {return '<span class="dot"></span>';}
  		});
    }

    if($gallery_six.length)
    {
      var _speed6 = ($gallery_six.attr("data-speed").length)?$gallery_six.attr("data-speed"):_default_speed;

      $gallery_six.slick({
    		autoplay:true,
    		slide:".slide-item",
    		autoplaySpeed:parseInt(_speed6),
        speed:1000,
    		arrows:false,
    		dots:true,
        fade:true,
    		infinite:true,
        rows: 0,
    		slidesToShow:1,
    		slidesToScroll:1,
        lazyload:'ondemand',
        prevArrow:$(".prev-6"),
        nextArrow:$(".next-6"),
        customPaging : function(slider, i) {return '<span class="dot"></span>';}
  		});
    }

    if($gallery_7.length)
    {
      var _speed7 = ($gallery_7.attr("data-speed").length)?$gallery_7.attr("data-speed"):_default_speed;

      $gallery_7.slick({
    		autoplay:true,
    		slide:".slide-item",
    		autoplaySpeed:parseInt(_speed7),
        speed:1000,
    		arrows:false,
    		dots:true,
        fade:true,
    		infinite:true,
        rows: 0,
    		slidesToShow:1,
    		slidesToScroll:1,
        lazyload:'ondemand',
        prevArrow:$(".prev-7"),
        nextArrow:$(".next-7"),
        customPaging : function(slider, i) {return '<span class="dot"></span>';}
  		});
    }

    if($gallery_8.length)
    {
      var _speed8 = ($gallery_8.attr("data-speed").length)?$gallery_8.attr("data-speed"):_default_speed;

      $gallery_8.slick({
    		autoplay:true,
    		slide:".slide-item",
    		autoplaySpeed:parseInt(_speed8),
        speed:1000,
    		arrows:false,
    		dots:true,
        fade:true,
    		infinite:true,
        rows: 0,
    		slidesToShow:1,
    		slidesToScroll:1,
        lazyload:'ondemand',
        prevArrow:$(".prev-8"),
        nextArrow:$(".next-8"),
        customPaging : function(slider, i) {return '<span class="dot"></span>';}
  		});
    }

    if($gallery_9.length)
    {
      var _speed9 = ($gallery_9.attr("data-speed").length)?$gallery_9.attr("data-speed"):_default_speed;

      $gallery_9.slick({
    		autoplay:true,
    		slide:".slide-item",
    		autoplaySpeed:parseInt(_speed9),
        speed:1000,
    		arrows:false,
    		dots:true,
        fade:true,
    		infinite:true,
        rows: 0,
    		slidesToShow:1,
    		slidesToScroll:1,
        lazyload:'ondemand',
        prevArrow:$(".prev-9"),
        nextArrow:$(".next-9"),
        customPaging : function(slider, i) {return '<span class="dot"></span>';}
  		});
    }

    if($gallery_10.length)
    {
      var _speed10 = ($gallery_10.attr("data-speed").length)?$gallery_10.attr("data-speed"):_default_speed;

      $gallery_10.slick({
    		autoplay:true,
    		slide:".slide-item",
    		autoplaySpeed:parseInt(_speed10),
        speed:1000,
    		arrows:false,
    		dots:true,
        fade:true,
    		infinite:true,
        rows: 0,
    		slidesToShow:1,
    		slidesToScroll:1,
        lazyload:'ondemand',
        prevArrow:$(".prev-10"),
        nextArrow:$(".next-10"),
        customPaging : function(slider, i) {return '<span class="dot"></span>';}
  		});
    }

    if($gallery_11.length)
    {
      var _speed11 = ($gallery_11.attr("data-speed").length)?$gallery_11.attr("data-speed"):_default_speed;

      $gallery_11.slick({
    		autoplay:true,
    		slide:".slide-item",
    		autoplaySpeed:parseInt(_speed11),
        speed:1000,
    		arrows:false,
    		dots:true,
        fade:true,
    		infinite:true,
        rows: 0,
    		slidesToShow:1,
    		slidesToScroll:1,
        lazyload:'ondemand',
        prevArrow:$(".prev-10"),
        nextArrow:$(".next-10"),
        customPaging : function(slider, i) {return '<span class="dot"></span>';}
  		});
    }
  }
};


/*-----------------------------------------------------------------------------------------
  FN: Slideshow
-----------------------------------------------------------------------------------------*/
$.fn.hero_slideshow = function()
{
  var $target = this;

  if($target.length)
  {
    $(window).load(function()
    {
      $target.addClass("loaded");

      $target.slick({
       slidesToShow: 1,
       slidesToScroll: 1,
       slide: ".slide-item",
       autoplay: true,
       autoplaySpeed: 6000,
       speed: 800,
       rows: 0,
       arrows: false,
       dots: true,
       fade: false,
       adaptiveHeight: true,
       infinite: true,
       customPaging : function(slider, i) {return '<span class="dot"></span>';}
     });
    });
  }
};


  /* Product Slider
  --------------------------------------------------------------------*/
  $.fn.productSlideshow = function()
  {
    var $target = $(this);

    if($target.length)
    {
      $target.slick({
        slidesToShow: 4,
        slidesToScroll: 4,
        arrows: true,
        dots: true,
        fade: false,
        slide: '.slide-item',
        rows: 0,
        prevArrow: $('.project-slide-arrow-left'),
        nextArrow: $('.project-slide-arrow-right'),
        customPaging : function(slider, i) {return '<span class="dot"></span>';},
        responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
          },
        },
        {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      }],
      });
    }
  };

/*-------------------------------------------------------------------------------
    Scrolling
-------------------------------------------------------------------------------*/
$.fn.headerScrollingFN = function()
{
  var $target = $(this);

  if($target.length)
  {
    var _height = $target.height()+100,
        $body = $("body"),
        $window = $(window),
        $pos = "";

    $window.scroll(function()
    {
       $pos = $window.scrollTop();

       if($pos>=_height)
       {
         $body.addClass("scrolling-init");
       } else {
         $body.removeClass("scrolling-init");
       }
    });
  }
};


/*-------------------------------------------------------------------------------
    Scroll To
-------------------------------------------------------------------------------*/
$.fn.scrollToElement = function()
{
  var $target = $(this);

  if($target.length)
  {
    $('.scroll-to:not(.scroll-to-on)').each(function()
  	{
  		$(this).click(function(e)
  		{
  			e.preventDefault();

        var $this = $(this);
  			var id = ($this[0].hasAttribute('href'))?$this.attr('href'):$this.attr('data-target');
  			var offset = ($this.data('offset'))? parseInt($this.data('offset')) : 0;
  			var speed = ($this.data('speed'))? parseInt($this.data('speed')) : 800;
  			var top = $(id).offset().top + offset;

  			// animation
  			if(!$('html:animated, body:animated').length)
        {
          $('html,body').animate({scrollTop: top},speed);
        }
  		});

  		$(this).addClass('scroll-to-on');
  	});
  }
};


/* Mobile Menu
   --------------------------------------------------------------------*/
  $.fn.showMobileMenu = function () {
    var $target = $(this);
    $target.on('click', function(){
      $('.hamburger').toggleClass('is-active');
      $('#mobile-menu-wrap').toggleClass('is-open');
    });
  };


  /* Header search functionality
  --------------------------------------------------------------------*/
  $.fn.headerSearchIcon = function() {

    var $icon   = $(this);
    var $menu   = $('#main-header-top');
    var $search = $('#header-search-form');

    $icon.on('click', function (e)
    {
      e.preventDefault();

      $icon.toggleClass('is-open');
      $menu.toggleClass('search-open');
      $search.toggleClass('is-open');

      if($(window).width()<=767){
        if($search.is(':visible')){
          $search.slideUp();
        } else {
          $search.slideDown();
        }
      }
    });
  };


  /* Hover Effect
  ---------------------------------------------------------------*/
  $.fn.hoverIntentPlugin = function()
  {
    var $target = $(this);


    if($target.length)
    {
      var $window = $(window);

      $target.find('a:first').click(function(e){ e.preventDefault(); return false; });

      $target.hoverIntent({
        sensitivity: 1,
        interval: 10,
        timeout: 100,
        over:function(){
          if($window.width() <= 767)
          {
            $(".sub-menu:first",this).removeClass("hoverOut").toggleClass("hoverIn").slideDown(600);
          } else {
            $(".sub-menu:first",this).removeClass("hoverOut").toggleClass("hoverIn").fadeIn();
          }
        },
        out: function(){
          if($window.width() <= 767)
          {
            $(".sub-menu:first",this).removeClass("hoverIn").toggleClass("hoverOut").slideUp(600);
          } else {
            $(".sub-menu:first",this).removeClass("hoverIn").toggleClass("hoverOut").fadeOut();
          }
        }
      });
    }
  };



/* Mobile menu animation trigger
---------------------------------------------------------------*/
$.fn.openMenuToggle = function()
{
  var $target   = $(this);

  if($target.length)
  {
    $target.click(function(e)
    {
      e.preventDefault();

      if($('body').hasClass('mini-cart-opened'))
      {
        $('body').removeClass('mini-cart-opened');
        $('#mini-cart').removeClass('menu-open');
        $('.site-container-cart').removeClass('menu-open');
      }
      else
      {
        $('body').addClass('mini-cart-opened');
        $('#mini-cart').addClass('spider-toggle');

        setTimeout(function()
        {
          $('#mini-cart').removeClass('spider-toggle');
        },3800);

        $('#mini-cart').addClass('menu-open');
        $('.site-container-cart').addClass('menu-open');
      }
    });

    //Close menu if click outside the mobile menu
    $('.main-content[role="document"]').on({
      click: function()
      {
        if($('body').hasClass('mini-cart-opened'))
        {
          $("#close-cart-btn").click();
        }
      }
    },":not(#mini-cart)");
  }
};


/*==============================================================================
    Woocommerce: Shop Mobile nav
==============================================================================*/
jQuery.fn.KIRADONJEWEL_CartToggle = function()
{
  var jQuerytarget = jQuery(this);

  if(jQuerytarget.length)
  {
    var jQuerycart = jQuery("#mini-cart");

    jQuerytarget.click(function()
    {
      if(jQuery(this).hasClass("active"))
      {
        jQuery(this).removeClass("active");
        jQuerycart.removeClass("active");
      }
      else
      {
        jQuery(this).addClass("active");
        jQuerycart.addClass("active");
      }

    });
  }
};

/*==============================================================================
    Woocommerce: Shop Mobile nav
==============================================================================*/
jQuery.fn.KIRADONJEWEL_MobileNav = function()
{
  var jQuerytarget = jQuery(this);

  if(jQuerytarget.length)
  {
    //Vars
    var jQuerytoggle = jQuerytarget.find(".toggle-button");
    var jQuerymenu_link = jQuerytarget.find("li a");

    //Toggle
    jQuerytoggle.click(function()
    {
      if(jQuerytarget.hasClass("active"))
      {
        jQuerytarget.removeClass("active");
      } else {
        jQuerytarget.addClass("active");
      }

      return false;
    });

    //Scrolling
    jQuerytarget.on("click","a",{}, function(e)
    {
      e.preventDefault();

      var _id = jQuery(this).text();
      var jQueryproducts = jQuery("#content .product-loop-container");
      var jQuerytarget_menu = jQueryproducts.find('.product-menu[data-id="'+_id+'"]');

      if(jQuerytarget_menu.length)
      {
        setTimeout(function()
        {
          jQuery("body,html").animate({
            scrollTop: jQuerytarget_menu.offset().top - 115
          },400).promise().done(function()
          {
            jQuerytarget.toggleClass("active");
          });
        },200);
      }
      else
      {
        alert("Error! We couldn't find the menu you are looking for.");
      }

      return false;
    });

  }
};


/*==============================================================================
    zerozero: Mini cart
==============================================================================*/
jQuery.fn.KIRADONJEWEL_miniCart = function()
{
  var jQuerytarget = jQuery(this);

  if(jQuerytarget.length)
  {
    //Trigger if there is items in the moini-cart
    if(jQuerytarget.find('.mini_cart_item').length)
    {
      //Callbacks init
      //jQuerytarget.MiniCart_UpdateOrder();
      //jQuerytarget.MiniCart_QtyInput();
      jQuerytarget.MiniCart_RemoveItem();
    }

    $('#close-cart-btn').openMenuToggle();
  }
};


/*==============================================================================
    Woocommerce: MiniCart: Update cart html
==============================================================================*/
jQuery.fn.MiniCart_AjaxRequest = function(cart_notification)
{
  var jQuerytarget = jQuery(this);

  if(jQuerytarget.length)
  {
     var jQuerycart = jQuery("#mini-cart");
     var _notification_msg = cart_notification || null;

     //alert(jQuerycart.find('input[name="_wp_http_referer"]').attr("value"));

     var data = {
       'action': 'mode_theme_update_mini_cart'
     };

     //HTTP REFERER
     var _wp_http_referer = $("#mini-cart").find('input[name="_wp_http_referer"]').val() || "/";

     jQuerycart.removeClass("updated");

     jQuery.post(woocommerce_params.ajax_url,data,function(response)
     {
       var html_cart = jQuery(response).find("#ajax-mini-cart-data").html();
       var _cart_count = jQuery(response).find("#ajax-mini-cart-data").attr('data-count');
       var jQuerymini_cart = jQuery("#ajax-mini-cart-data");

       if($('#starfield').length){
         setTimeout(foolmoon, 200);
       }

       //Append data
       jQuerymini_cart.html(html_cart);

       //Append total items to cart
       $('#ajax-cart-counter').text(_cart_count);

       //Callback events
       jQuerymini_cart.KIRADONJEWEL_miniCart();

       //HTTP Referer
       jQuerymini_cart.find('input[name="_wp_http_referer"]').val(_wp_http_referer);

       //Loader
       jQuery("#cart-loader").removeClass("active");
       jQuerycart.addClass("updated");

       //Update price
       /*jQuery("#ajax-price").html(jQuery("#get-price").html() || "0");

       //Animate cart to bottom
       if(jQuerymini_cart.find(".cart-review-order").length)
       {
         jQuerymini_cart.animate({scrollTop: jQuerymini_cart.find(".cart-review-order").offset().top},1600);
       }*/

       //If notification exisit
       if(_notification_msg)
       {
         var jQuerynotification = jQuery("#cart-notifications");

         jQuerynotification.text(_notification_msg);

         //slide Down
         setTimeout(function() { jQuerynotification.slideDown(); },400);

         //Slide Up
         setTimeout(function() { jQuerynotification.slideUp(); },2400);
       }

       //Animate spider
       setTimeout(function()
       {
         $('#mini-cart').addClass('spider-toggle');

         setTimeout(function()
         {
           $('#mini-cart').removeClass('spider-toggle');
         },3800);
       },3000);

     });
  }
  else { alert("We couldn't update the card."); }
};


/*==============================================================================
    Woocommerce: MiniCart: Update cart quanities
==============================================================================*/
jQuery.fn.MiniCart_UpdateOrder = function()
{
  var jQuerytarget = jQuery(this);

  if(jQuerytarget.length)
  {
    var jQueryform = jQuerytarget.find("form");

    jQueryform.on("click", ".mini-cart-update", {} ,function(e)
    {
      e.preventDefault();

      var action = jQueryform.attr("action");
      var serialize_data = jQueryform.serialize();
      var cart_notification = jQuery("#cart-notifications").attr("data-success") || null;

      //Loader
      jQuery("#cart-loader").addClass("active");

       jQuery.post(action,serialize_data,function(response)
       {
         //var jQuerycart = jQuery(response).find("#ajax-mini-cart-data").html();
         jQuery("#ajax-mini-cart-data").MiniCart_AjaxRequest(cart_notification);
       });

       return false;
    });
  }
};


/*==============================================================================
    Woocommerce: CALLBACK: Remove Item
==============================================================================*/
jQuery.fn.MiniCart_RemoveItem = function()
{
  var jQuerytarget = jQuery(this);

  if(jQuerytarget.length)
  {

    jQuerytarget.off().on("click", ".remove-item", {}, function(e)
    {
      e.preventDefault();

      //Loader
      jQuery("#cart-loader").addClass("active");

      var cart_notification = jQuery("#cart-notifications").attr("data-remove") || null;

      var data = {
         'action': 'remove_item_from_cart',
         'product_id' : jQuery(this).attr("data-key")
       };

       jQuery.post(woocommerce_params.ajax_url,data,function(remove)
       {
         jQuery("#ajax-mini-cart-data").MiniCart_AjaxRequest(cart_notification);
       });

      return false;
    });
  }
  else { alert("We couldn't update the card."); }
};



/*==============================================================================
    Woocommerce: MiniCart: QTY
==============================================================================*/
jQuery.fn.MiniCart_QtyInput = function()
{
  var jQuerytarget = jQuery(this);

  if(jQuerytarget.length)
  {
    var jQuerycart_list = jQuerytarget.find(".cart_list");
    var _val = '';

    jQuerycart_list.on("change keyup", ".qty", {}, function()
    {
      _val = this.value;

      if (_val === "0")
      {
        _val = "1";
      }

      jQuery(this).attr('value',_val).data('value',_val).val(_val);

      return false;
    });
  }
};



/*-------------------------------------------------------------------------------
    Visible Element
-------------------------------------------------------------------------------*/
$.fn.visibleElementFN = function()
{
  var $target = $(this);

  if($target.length)
  {
    $(window).scroll(function()
		{
		  $target.each(function(i, el)
		  {
		    var _el = $(el);

		    if (_el.onViewportFN(true))
		    {
		      _el.addClass("element-visible");
		    }
		  });
		});
  }
};


/*-------------------------------------------------------------------------------
    Scroll Down Header Hide
-------------------------------------------------------------------------------*/
$.fn.hideHeader_onScrollingDown = function()
{
  //Target body
  var $target = $(this);

  if($target.length)
  {
    var $window = $(window);
    var $headerToHide = $('#main-header');


    //Handle scrolling if bigger than 1000
    if($window.width() > 320)
  	{
      //Reset vars
      var lastScrollTop       = 0,
          initTop             = 0,
          changeDirection     = false,
          gutterOffset        = 480, //change offset
          lastDirectionDown   = false;

      //Height
      var headerHeight = $headerToHide.height();

      $window.scroll(function ()
      {
        if(!$('body').hasClass('menu-opened'))
        {
          var thisScrollTop = $(this).scrollTop();
              changeDirection = ( thisScrollTop > gutterOffset && (thisScrollTop > lastScrollTop && lastDirectionDown === false) || (thisScrollTop < lastScrollTop && lastDirectionDown === true) );

          if (changeDirection === true)
          {
            //Class for css if needed
            $headerToHide.toggleClass('scrolling-header');

            lastDirectionDown = ( lastDirectionDown === false );
          }

          $headerToHide.css( {
            'top': $headerToHide.hasClass('scrolling-header') ? (-1) * headerHeight : initTop
          });

          lastScrollTop = thisScrollTop;
        }
      });
    }
  }
};


/*-------------------------------------------------------------------------------
    On Viewport Element - Visible
-------------------------------------------------------------------------------*/
$.fn.onViewportFN = function(partial)
{
  var $t            = $(this),
      $w            = $(window),
      viewTop       = $w.scrollTop(),
      viewBottom    = viewTop + $w.height(),
      _delay        = 200,
      _top          = $t.offset().top+_delay,
      _bottom       = _top + $t.height(),
      compareTop    = partial === true ? _bottom : _top,
      compareBottom = partial === true ? _top : _bottom;

return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
};

/*-------------------------------------------------------------------------------
    Touched Bottom
-------------------------------------------------------------------------------*/
$.fn.touchBottomFN = function()
{
  //Target body
  var $target = $(this);

  if($target.length)
  {
    var $window = $(window), $document = $(document);

    $window.scroll(function()
		{
			var beforeBottom = 540,
          _height = $document.height()-beforeBottom,
          w_height = $window.height(),
          total = $window.scrollTop()+w_height;

       if(_height<total)
       {
         $target.addClass("touched-bottom");
       } else {
	       $target.removeClass("touched-bottom");
       }
	   });
  }
};



/*-------------------------------------------------------------------------------
    FAQ - Item
-------------------------------------------------------------------------------*/
$.fn.faqToggle = function()
{
  var $target = $(this);

  if($target.length)
  {
    var $question = $target.find(".faq-question");

    $question.each(function()
    {
      $(this).click(function(e)
      {
        e.preventDefault();

        var $this_wrapper = $(this).parents(".faq-item").eq(0);

        if(!$this_wrapper.hasClass('active'))
        {
          $(".accordion-wrapper .active").find('.faq-answer').slideUp(360);
          $(".accordion-wrapper .active").removeClass('active');

          $this_wrapper.find(".faq-answer").slideDown(380);
          $this_wrapper.addClass("active");

        } else {
          $this_wrapper.find(".faq-answer").slideUp(380);
          $this_wrapper.removeClass("active");
        }

        setTimeout(function()
        {
          var _offset = 70;

          $this_wrapper.scrollToSection(_offset);
        },440);

        return false;
      });
    });
  }
};




/*-------------------------------------------------------------------------------
    Footer Toggle
-------------------------------------------------------------------------------*/
$.fn.footerToggle = function()
{
  var $target = $(this);

  if($target.length)
  {
    if($(window).width()<=800)
    {
      $target.click(function(e)
      {
        e.preventDefault();

        var $this_wrapper;

        $this_wrapper = $(this).parents(".col").eq(0);
        var $visible = $this_wrapper.find('.wrap');

        if($visible.is(':visible'))
        {
          $visible.slideUp(380);
          $this_wrapper.removeClass("active");
        } else {
          $visible.slideDown(380);
          $this_wrapper.addClass("active");
        }
        return false;
      });
    }
  }
};


/*------------------------------------------------------------------------------------
    Share this
-------------------------------------------------------------------------------------*/
$.fn.shareThis = function()
{
  var $target = $(this);

  if($target.length)
  {
    $target.click(function(e)
    {
      e.preventDefault();

      var $wrapper = $(this).parents(".share-buttons");

      $wrapper.toggleClass("active");
    });
  }
};


/*----------------------------------------------------------------------------------
                    Callback: Render HTML
-----------------------------------------------------------------------------------*/
var renderHtml = function(html)
{
 var result = String(html).replace(/<\!DOCTYPE[^>]*>/i, '')
              .replace(/<(html|head|body|title|script)([\s\>])/gi,'<div id="get-html-$1"$2')
              .replace(/<\/(html|head|body|title|script)\>/gi,'</div>');

 return result;
};


/*----------------------------------------------------------------------------------
                    FN: Load More
-----------------------------------------------------------------------------------*/
$.fn.ajaxLoadMore = function()
{
 //Target pagination
 var $loadMore = $(this);

 if($loadMore.length)
 {
   //Target button
   var $nextBtn = $loadMore.find(".nav-previous > a");

   $nextBtn.click(function( e )
   {
       e.preventDefault();

       //Show spinner
       $loadMore.toggleClass("active");

       //Defaults
       var $page = $(this).attr("href"),
           $last_item = $(".paging-content:last"),
           $next_link = $(this);

         $.ajax({
           url: $page,
           success: function( html, textStatus, jqXHR )
           {
             var $data	= $(renderHtml(html)),
                 $next_href = $data.find(".nav-previous > a").attr("href"),
                 $posts = $data.find(".paging-content").html();

             //Get href for next
             $nextBtn.attr("href", $next_href);

             //Append items
             $last_item.after('<div class="paging-content ajax-paging">'+$posts+'</div>');

             //hide pagination if there is no more pages or toggle spinner
             if($data.find(".nav-previous > a").length)
             {
               $loadMore.toggleClass("active");
             }
             else
             {
               $loadMore.hide().toggleClass("active").addClass("no-more-paging");
             }

           },
           error: function( jqXHR, textStatus, errorThrown )
           {
             //Error
             alert( 'The following error occured: ' + textStatus +' Try to refresh the page or contact us. ' + errorThrown);
           },
           complete: function( jqXHR, textStatus, errorThrown )
           {
             //callbacks
             $(".ajax-post:not(.active)").getAjaxPost();
           }
         });

         return false;

   });//End .click()
 }
};



/*----------------------------------------------------------------------------------
                    FN: Load More
-----------------------------------------------------------------------------------*/
$.fn.productsLoadMore = function()
{
 //Target pagination
 var $loadMore = $(this);

 if($loadMore.length)
 {
   //Target button
   var $nextBtn = $loadMore.find(".load-more-products");

   //hide pagination
   $('.page-numbers').hide();

   if($(".page-numbers .next").length)
   {
     $loadMore.show();
   } else {
     $loadMore.hide();
   }

   //Click
   $nextBtn.click(function( e )
   {
       e.preventDefault();

       var urlParams = new URLSearchParams(window.location.search);

       //Show spinner
       $loadMore.toggleClass("active");

       //Defaults
       var $page = parseInt($(this).attr("data-page")),
           $last_product = $("li.product:last"),
           $pagination_next = $(".page-numbers").find('.next'),
           $and_or = (urlParams.get('orderby'))?'&':'?';

       if($pagination_next.length)
       {
         var $paged_num = $page+1;

         $.ajax({
           url: window.location.href+$and_or+'product-page='+$paged_num,
           success: function( html, textStatus, jqXHR )
           {
             var $data	= $(renderHtml(html)),
                 $products = $data.find("ul.products").html();

             //Get href for next
             $nextBtn.attr("data-page", $paged_num);

             //Append items
             $last_product.after($products);

             //hide pagination if there is no more pages or toggle spinner
             if($data.find(".page-numbers .next").length)
             {
               $loadMore.toggleClass("active");
             }
             else
             {
               $loadMore.hide().toggleClass("active").addClass("no-more-paging");
             }

           },
           error: function( jqXHR, textStatus, errorThrown )
           {
             //Error
             alert( 'The following error occured: ' + textStatus +' Try to refresh the page or contact us. ' + errorThrown);
           },
           complete: function( jqXHR, textStatus, errorThrown ){}
         });//ajax
       }
       else
       {
         $loadMore.hide();
       }

      return false;
   });//End .click()
 }
};


/* Shortcodes: get post
----------------------------------------------------------------*/
$.fn.getAjaxPost = function()
{
  var $target = $(this);

  if($target.length)
  {
    $target.off('click');
    $target.each(function()
    {
      $(this).on('click',function(e)
    	{
        e.preventDefault();

        var $this = $(this),
            _uri = $this.attr("href"),
            $wrapper = $("#append-post"),
            $header = $("#main-header"),
            _height = $header.height()+28,
  					$History = window.History,
  					$blog_title = document.title;

    			$History.pushState(null,$blog_title,decodeURIComponent(_uri));

          //Loader
          $header.toggleClass("loader");

    			$("html,body").animate({
    				scrollTop: $wrapper.offset().top-155
    			},900).promise().done(function()
    			{
            $.ajax({
              url: _uri,
              success: function( html, textStatus, jqXHR )
              {
                var $data	= $(renderHtml(html)),
                    $post_html = $data.find("#append-post").html();

                  $wrapper.html($post_html).addClass("animate-post");
              },
              complete: function( jqXHR, textStatus )
              {
                //Loader
                $header.toggleClass("loader");

                //CALLBACKS
                $(".share-buttons > .anchor-hover").shareThis();
              },
              error: function( jqXHR, textStatus, errorThrown )
              {
                //Loader
                $header.toggleClass("loader");

                alert(jqXHR+textStatus+errorThrown);
              }
            });
          });

        return false;

      });//end click
    });//end each
  }//if $target
};
