<?php
  get_template_part('templates/flexible-content/whisk-fx-init');
?>

<?php
global $post;
global $post_type;

  $back_href = "/lookbooks";

  //Get first post
  $args = array(
    'post_type' => $post_type,
    'orderby' => 'menu_order',
    'order' => 'ASC',
    'posts_per_page' => -1
  );

  //Prev next
	$prevPost = get_previous_post();
	$nextPost = get_next_post();

	$parents = get_post_ancestors( $post->ID );

	$prevTitle = get_the_title($prevPost->ID);
	$prevLink = get_permalink($prevPost->ID);
  $prevThumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($prevPost->ID,'full'));


	$nextTitle = get_the_title($nextPost->ID);
	$nextLink = get_permalink($nextPost->ID);
  $nextThumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($nextPost->ID,'full'));

  $posts_array = get_posts($args);

  $first_item = $posts_array[0];
  $last_item = $posts_array[count($posts_array) -1];



  $pdf = get_field('download_pdf');
?>

<?php
if(have_posts()):
  while (have_posts()) : the_post();
  $ID = get_the_id();
  $content = apply_filters( 'the_content', get_the_content() );
  if(!empty($content)):
  ?>
  <article class="entry-content lookbook-entry-content">
    <?php echo $content; ?>
  </article>
  <?php endif; ?>

  

  <?php if(!empty($pdf)): ?>
  <nav class="download-pdf">
    <a class="btn button" target="_blank" href="<?= $pdf['url']; ?>">Download PDF</a>
  </nav>
  <?php endif; ?>
  <?php
  endwhile;
endif;
?>

<nav id="post-nav" class="call-to-action clearfix">

  <?php if(!empty($prevPost)): ?>
    <a class="prev-post item js-redirect" href="<?= $prevLink; ?>" data-title="<?= $prevTitle; ?>">
      <h4><span class="ent-arr">&larr;</span> <?= $prevTitle; ?></h4>
    </a>
  <?php else: ?>
    <a class="first-item-active prev-post arrows item js-redirect" href="<?= get_the_permalink($last_item->ID); ?>" data-title="<?= get_the_title($last_item->ID); ?>">
      <h4><span class="ent-arr">&larr;</span> <?= get_the_title($last_item->ID); ?></h4>
    </a>
  <?php endif; ?>

  <a class="all item js-redirect" href="<?= get_home_url(); ?><?= $back_href; ?>">View all</a>

  <?php if(!empty($nextPost)): ?>
    <a class="next-post arrows item js-redirect" href="<?= $nextLink; ?>" data-title="<?= $nextTitle; ?>">
      <h4><?= $nextTitle; ?> <span class="ent-arr">&rarr;</span></h4>
    </a>
  <?php else: ?>
    <a class="last-item-active next-post arrows item js-redirect" href="<?= get_the_permalink($first_item->ID); ?>" data-title="<?= get_the_title($first_item->ID); ?>">
      <h4><?= get_the_title($first_item->ID); ?> <span class="ent-arr">&rarr;</span></h4>
    </a>
  <?php endif; ?>
</nav>
