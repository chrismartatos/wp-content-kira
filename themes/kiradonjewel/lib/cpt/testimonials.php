<?php

add_action( 'init', 'register_cpt_testimonials' );


function register_cpt_testimonials()
{
    $singular = 'Testimonial';
    $singular_lowercase = 'testimonial';
    $plural = 'Testimonials';
    $slug = 'testimonials';
    $post_type = 'testimonials';
    $desc = 'Clients testimonials';
    $supports = array('thumbnail', 'title', 'editor', 'page-attributes', 'paged');

    //Labels
    $labels = array(
    'name' => _x( $plural, 'post type general name'),
    'singular_name' => _x( $singular, 'post type singular name'),
    'add_new' => _x('Add New', $singular_lowercase ),
    'add_new_item' => __('Add New '. $singular_lowercase),
    'edit_item' => __('Edit '. $singular_lowercase ),
    'new_item' => __('New '. $singular_lowercase ),
    'view_item' => __('View '. $singular_lowercase),
    'search_items' => __('Search '. $plural),
    'not_found' =>  __('No '. $singular .' found'),
    'not_found_in_trash' => __('No '. $singular .' found in Trash'),
    'parent_item_colon' => '',
    'menu_name' => $plural
    );

    //Args
    $args = array(
    'labels' => $labels,
    'description' => $desc,
    'public' => true,
    'menu_position' => 21,
    'publicly_queryable' => false,
    'show_ui' => true,
    'show_in_menu' => true,
    'show_in_nav_menus' => false,
    'show_in_rest' => false,
    'query_var' => false,
    'menu_icon' => 'dashicons-testimonial',
    'rewrite' => Array('slug'=> $slug ),
    'capability_type' => 'post',
    'has_archive' => false,
    'exclude_from_search' => true,
    'hierarchical' => false,
    'supports' => $supports
    );

    register_post_type( $post_type, $args );
}
