<?php


/*-------------------------------------------------------------------------------------
      Price next to variation
-------------------------------------------------------------------------------------*/
//add_filter( 'woocommerce_variation_option_name', 'display_price_in_variation_option_name' );

function display_price_in_variation_option_name( $term ) {
    global $wpdb, $product;

    $result = $wpdb->get_col( "SELECT slug FROM {$wpdb->prefix}terms WHERE name = '$term'" );

    $term_slug = ( !empty( $result ) ) ? $result[0] : $term;


    $query = "SELECT postmeta.post_id AS product_id
                FROM {$wpdb->prefix}postmeta AS postmeta
                    LEFT JOIN {$wpdb->prefix}posts AS products ON ( products.ID = postmeta.post_id )
                WHERE postmeta.meta_key LIKE 'attribute_%'
                    AND postmeta.meta_value = '$term_slug'
                    AND products.post_parent = $product->id";

    $variation_id = $wpdb->get_col( $query );

    $parent = wp_get_post_parent_id( $variation_id[0] );

    if ( $parent > 0 ) {
        $_product = new WC_Product_Variation( $variation_id[0] );
        $_currency = get_woocommerce_currency_symbol();
        return $term . ' '.$_currency.''. $_product->get_price()  . '';
    }
    return $term;

}


/*-------------------------------------------------------------------------------------
      loop
-------------------------------------------------------------------------------------*/
add_action( 'woocommerce_before_shop_loop_item_title', 'loop_product_thumb_open', 5, 2);
add_action( 'woocommerce_before_shop_loop_item_title', 'loop_product_thumb_close', 12, 2);
add_action( 'woocommerce_before_subcategory_title', 'loop_product_thumb_open', 5, 2);
add_action( 'woocommerce_before_subcategory_title', 'loop_product_thumb_close', 12, 2);
/**
* Add wrap around all product images.
*
*/
function loop_product_thumb_open($post)
{
    global $post;

    $product = wc_get_product( $post->ID );
    $attachment_ids = $product->get_gallery_attachment_ids();

    if(!empty($attachment_ids))
    {
      $class = (!empty($attachment_ids[0]))? 'hover-image': 'hover-empty';

      echo '<div class="clearfix product-thumb-hover-wrap '.$class.'" data-id="'.$attachment_ids[0].'">';
      echo wp_get_attachment_image($attachment_ids[0],'shop_catalog',false,'data-hover');
    }

}

function loop_product_thumb_close()
{
  global $post;

  $product = wc_get_product( $post->ID );
  $attachment_ids = $product->get_gallery_attachment_ids();

  if(!empty($attachment_ids))
  {
    echo '</div>';
  }

}
