<?php

/**
 * Add a custom product data tab
 */
add_filter( 'woocommerce_product_tabs', 'woo_shipping_product_tab' );

function woo_shipping_product_tab( $tabs )
{
	global $post;

	$terms = get_the_terms( $post, 'product_cat');

	$tabs['shipping_tab'] = array(
		'title' 	=> __( 'Shipping', 'woocommerce' ),
		'priority' 	=> 50,
		'callback' 	=> 'woo_shipping_product_tab_content'
	);

	//Tabs
	if(!empty($terms)):
		foreach($terms as $term):
			if($term->slug=='rings'):
				$tabs['payment_tab'] = array(
					'title' 	=> __( 'Ring Sizes', 'woocommerce' ),
					'priority' 	=> 60,
					'callback' 	=> 'woo_ring_sizes_tab_content'
				);
			endif;
		endforeach;
	endif;

	return $tabs;
}

function woo_shipping_product_tab_content()
{
  $page_shipping = get_post(57);

  echo '<div class="custom-tab-content">'.$page_shipping->post_content.'</div>';
}


function woo_payment_product_tab_content()
{
  $page_payment = get_post(60);

  echo '<div class="custom-tab-content">'.$page_payment->post_content.'</div>';
}



function woo_ring_sizes_tab_content()
{
	$get_content = get_field('single_product_content', 'product_cat_17');

	if(!empty($get_content)):
	echo '<div data-term-id="17" class="custom-content">'.$get_content.'</div>';
	endif;
}



/**
 * CUSTOM FIELD
 */
add_action('woocommerce_product_meta_end','add_ring_sizes' );

function add_ring_sizes()
{
	global $post;

	$terms = get_the_terms( $post, 'product_cat');

	//Tabs
	if(!empty($terms)):
		foreach($terms as $term):
			$get_content = get_field('single_product_content', 'product_cat_'.$term->term_id);

			if(!empty($get_content)):
			echo '<div data-id="'.$term->term_id.'" class="custom-product-term" style="padding: 2rem 0 0;">'.$get_content.'</div>';
			endif;
		endforeach;
	endif;
}
