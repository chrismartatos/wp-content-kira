<?php

/*-----------------------------------------------------------------------------------
    AJAX - Mini cart HTML
-----------------------------------------------------------------------------------*/
add_filter( 'wp_ajax_nopriv_mode_theme_update_mini_cart', 'mode_theme_update_mini_cart' );
add_filter( 'wp_ajax_mode_theme_update_mini_cart', 'mode_theme_update_mini_cart' );
/* DEQUEUE */
add_action( 'wp_enqueue_scripts', 'dequeue_woocommerce_cart_fragments', 11);

function mode_theme_update_mini_cart()
{
  echo wc_get_template( 'cart/mini-cart.php' );

  die();
}

function dequeue_woocommerce_cart_fragments()
{
  wp_dequeue_script('wc-cart-fragments');
}

/*-----------------------------------------------------------------------------------
    AJAX - Mini cart - Remove Product
-----------------------------------------------------------------------------------*/
add_action('wp_ajax_nopriv_remove_item_from_cart', 'remove_item_from_cart');
add_action('wp_ajax_remove_item_from_cart', 'remove_item_from_cart');

function remove_item_from_cart($cart_id)
{
  global $wpdb, $woocommerce;

  session_start();

  $cart = WC()->instance()->cart;
  $cart_id = $_POST['product_id'];
  $cart_item_id = $cart->find_product_in_cart($cart_id);

  if($cart_item_id)
  {
     $cart->set_quantity($cart_item_id,0);

     return true;
  }

  exit();
}
