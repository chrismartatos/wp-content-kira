<?php
/*
****** Product Catalog Header
*/
add_filter( 'woocommerce_get_catalog_ordering_args', 'custom_woocommerce_get_catalog_ordering_args' );

 // Apply custom args to main query
function custom_woocommerce_get_catalog_ordering_args( $args ) {

	$orderby_value = isset( $_GET['orderby'] ) ? woocommerce_clean( $_GET['orderby'] ) : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) );

  $args['orderby'] = 'date';
  $args['order'] = 'DESC';

	return $args;
}


/*
****** Change Woo Product H1 Tag to H3 based on FX header
*/
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
add_action( 'woocommerce_single_product_summary', 'sgl_template_single_title', 5 );

function sgl_template_single_title()
{
	if(is_single()):

		if( have_rows('fx_page_header') ):
		    while ( have_rows('fx_page_header') ) : the_row();

				if( get_row_layout() == 'page_header' ):
					$title = get_sub_field('title');
					if(!empty($title)){
						the_title( '<h2 class="product_title entry-title">', '</h2>' );
					} else {
				    the_title( '<h1 class="product_title entry-title">', '</h1>' );
					}
				else:
					the_title( '<h1 class="product_title entry-title">', '</h1>' );
				endif;
		    endwhile;
		 else :
			// no flexible content
	    the_title( '<h1 class="product_title entry-title">', '</h1>' );
		endif;

	endif;//Single
}


/**
 * Remove & Crossels related products output
 */
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );

/*-----------------------------------------------------------------------------------
  // Load more - the woocommerce_after_shop_loop callback
-----------------------------------------------------------------------------------*/
function action_woocommerce_after_shop_loop()
{
  global $wp_query;

  echo '<div id="paging-products">
    <a href="#" data-page="1" class="load-more-products button btn">Load More <span class="css-loader"></span></a>
  </div>';
};

// add the action
add_action( 'woocommerce_after_shop_loop', 'action_woocommerce_after_shop_loop', 10, 2 );


/*-----------------------------------------------------------------------------------
    jet pack notifications
-----------------------------------------------------------------------------------*/
add_filter( 'jetpack_just_in_time_msgs', '__return_false' );
add_filter( 'woocommerce_helper_suppress_admin_notices', '__return_true' );


/*-----------------------------------------------------------------------------------
    Breadcrumbs
-----------------------------------------------------------------------------------*/
add_filter( 'woocommerce_breadcrumb_defaults', 'prefix_change_breadcrumb_home_text' );
/**
 * Rename "home" in WooCommerce breadcrumb
 */
function prefix_change_breadcrumb_home_text( $defaults ) {
    // Change the breadcrumb home text from 'Home' to 'Shop'
	$defaults['home'] = 'Shop';
  $defaults['delimiter'] = ' › ';

	return $defaults;
}

/**
 * Replace the home link URL in WooCommerce breadcrumb
 */
add_filter( 'woocommerce_breadcrumb_home_url', 'prefix_custom_breadrumb_home_url' );

function prefix_custom_breadrumb_home_url()
{
    //return '/products/jewels/all/';
    return get_site_url().'/products/new-in/';
}



/*-----------------------------------------------------------------------------------
    // Hide trailing zeros on prices.
-----------------------------------------------------------------------------------*/
add_filter( 'woocommerce_price_trim_zeros', 'wc_hide_trailing_zeros', 10, 1 );

function wc_hide_trailing_zeros( $trim ) {

    return true;

}

/*-----------------------------------------------------------------------------------
    Empty cart
-----------------------------------------------------------------------------------*/
add_action( 'wc_empty_cart_message', 'custom_add_content_empty_cart' );

function custom_add_content_empty_cart()
{
  $spider = '<div class="spider">
    <div class="spiderweb"></div>
    <div class="spider-body">
      <div class="eye left"></div>
      <div class="eye right"></div>
    </div>
    <div class="legs left">
      <div class="leg"></div>
      <div class="leg"></div>
      <div class="leg"></div>
    </div>
    <div class="legs right">
      <div class="leg"></div>
      <div class="leg"></div>
      <div class="leg"></div>
    </div>
  </div>';

  echo '<div class="custom-empty-cart" style="text-align: center;">'.$spider.'<p class="cart-empty">Your cart is currently empty.</p>
  <p><a class="button wc-backward" href="'.get_term_link( 16, 'product_cat' ).'">Return to shop</a></p></div>';
}
