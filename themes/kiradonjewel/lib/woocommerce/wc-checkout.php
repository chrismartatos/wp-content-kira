<?php
/* Billing Fields */
add_filter( 'woocommerce_billing_fields', 'kiradonjewel_wc_phone_field' );

function kiradonjewel_wc_phone_field( $fields )
{
    $fields['billing_phone']['required'] = true;
    $fields['billing_phone']['label'] = 'Phone (Required for postal services & delivery)';
    return $fields;
}


/* COUPON CODE POSITION ON CHECKOUT PAGE */
remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );

add_action( 'woocommerce_checkout_after_customer_details', 'woocommerce_checkout_coupon_form' );
