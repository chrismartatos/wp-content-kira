<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
 * Add <body> classes
 */

function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */

function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');


/* PHP - Function to build product data
---------------------------------------------------------------*/
function get_product_id($cart_item_key, $cart_item)
{
  global $wpdb, $woocommerce;

  // $_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
  // $product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

  $output['product'] = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
  $output['product_id'] = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

  return $output;

}


function build_product_values($product, $cart_item, $cart_item_key)
{

  if (! $product && $product->exists() ) {
    return false;
  }

  if ( $cart_item['quantity'] <= 0 ) {
    return false;
  }

  if ( apply_filters( 'woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
    $output['product_name'] = apply_filters( 'woocommerce_cart_item_name', $product->get_name(), $cart_item, $cart_item_key );
    $output['thumbnail'] = apply_filters( 'woocommerce_cart_item_thumbnail', $product->get_image(), $cart_item, $cart_item_key );
    $output['product_price'] = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $product ), $cart_item, $cart_item_key );
    $output['product_permalink'] = apply_filters( 'woocommerce_cart_item_permalink', $product->is_visible() ? $product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
  }

  return $output;

}

/**
 * Use Site Icon on Login pages
 */

function my_login_logo() { ?>
    <style type="text/css">
        body.login {
          background: #fff;
          padding-top: 8%;
          overflow: hidden;
        }
        #login {
          border: 1px solid #d1d1d1;
          padding: 15px!important;
        }
        #login h1 {
          margin-top: 30px;
        }
        #login h1 a, .login h1 a {
          background-image: url(<?php site_icon_url(); ?>);
          padding-bottom: 0;
        }
        #login form {
          -webkit-box-shadow: none;
          box-shadow: none;
        }
        .login #nav a:hover,
        .login #backtoblog a:hover,
        .login h1 a:hover {
          color: #4f4f4f!important;
        }
        input[type="text"]:focus {
          border-color: #828282!important;
          -webkit-box-shadow: 0 0 2px #828282!important;
          box-shadow: 0 0 2px #828282!important;
        }
        input[type="password"]:focus {
          border-color: #828282!important;
          -webkit-box-shadow: 0 0 2px #828282!important;
          box-shadow: 0 0 2px #828282!important;
        }
        input[type="checkbox"]:checked:before {
          color: #4f4f4f!important;
        }
        .wp-core-ui .button-primary {
          background: #828282!important;
          border-color: #828282!important;
          border-radius: 0!important;
          -webkit-box-shadow: 0 1px 0 #828282!important;
          box-shadow: 0 1px 0 #828282!important;
          color: #fff;
          text-decoration: none;
          text-shadow: none!important;
          transition: all ease-in-out .3s;
        }
        .wp-core-ui .button:hover {
          background: #4f4f4f!important;
          border-color: #4f4f4f!important;
          -webkit-box-shadow: 0 1px 0 #4f4f4f!important;
          box-shadow: 0 1px 0 #4f4f4f!important;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', __NAMESPACE__ . '\\my_login_logo', 100 );

/**
 * Use Site Address on Login logo
 */

function my_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', __NAMESPACE__ . '\\my_login_logo_url' );

function my_login_logo_url_title() {
    return 'Your Site Name and Info';
}
add_filter( 'login_headertitle', __NAMESPACE__ . '\\my_login_logo_url_title' );

/**
 * Remove WordPress logo from Admin page
 */

function annointed_admin_bar_remove() {
        global $wp_admin_bar;

        /* Remove their stuff */
        $wp_admin_bar->remove_menu('wp-logo');
}
add_action('wp_before_admin_bar_render',  __NAMESPACE__ . '\\annointed_admin_bar_remove', 0);


/*
 * Callback function to filter the MCE settings
 */

// Callback function to insert 'styleselect' into the $buttons array
function my_mce_buttons_2($buttons) {
    array_unshift($buttons, 'styleselect');
    return $buttons;
}

// Register our callback to the appropriate filter
add_filter('mce_buttons_2', __NAMESPACE__ . '\\my_mce_buttons_2');

function my_mce_before_init_insert_formats($init_array) {
// Define the style_formats array
    $style_formats = array(
        // Each array child is a format with it's own settings
        array(
            'title' => 'The Macambre',
            'inline' => 'span',
            'classes' => 'the-macambre'
        ),
        array(
            'title' => 'Text Large',
            'inline' => 'span',
            'classes' => 'txt-lg'
        ),
        array(
            'title' => 'Text Small',
            'inline' => 'span',
            'classes' => 'txt-sm'
        ),
        array(
            'title' => 'Text Dark',
            'inline' => 'span',
            'classes' => 'txt-dark'
        ),
        array(
            'title' => 'Text Light',
            'inline' => 'span',
            'classes' => 'txt-light'
        ),
        array(
            'title' => 'Text Primary',
            'inline' => 'span',
            'classes' => 'txt-primary'
        ),
        array(
            'title' => 'Text Secondary',
            'inline' => 'span',
            'classes' => 'txt-secondary'
        ),
        array(
            'title' => 'Button Primary',
            'inline' => 'a',
            'classes' => 'btn button'
        ),
        array(
            'title' => 'Button White',
            'inline' => 'a',
            'classes' => 'btn button white-button'
        ),
        array(
            'title' => 'Mobile Only',
            'inline' => 'span',
            'classes' => 'mobile-only'
        ),
        array(
            'title' => 'Desktop Only',
            'inline' => 'span',
            'classes' => 'desktop-only'
        ),
    );
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode($style_formats);
    return $init_array;
}

// Attach callback to 'tiny_mce_before_init'
add_filter('tiny_mce_before_init', __NAMESPACE__ .  '\\my_mce_before_init_insert_formats');

/**
 * Pull in SVG asset to be used inlne. Please check
 * the gulp task svgstore for more info on how
 * the SVG assests are being compiled
 *
 * @var  string  Filename without extension or folder prefixes
 *
 */
function include_svg( $name ) {

    $file = 'dist/svgs/'. $name .'.php';

    ob_start();
    $file_content = include(locate_template($file));
    $content = ob_get_clean();

    return $content;
}
