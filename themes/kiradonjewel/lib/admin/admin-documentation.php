<?php $image_src = get_stylesheet_directory_uri().'/lib/admin/screenshots/'; ?>

<section class="admin-docs cf">
	<h1>Documentation</h1>

	<nav class="admin-nav cf" id="sticly-menu">
		<ul>
      <li><a href="#welcome-to-wordpress">Welcome to WordPress</a></li>
      <li><a href="#users">Users</a></li>
      <li><a href="#posts">Posts</a></li>
      <li><a href="#pages">Pages</a></li>
      <li><a href="#custom-post-types">Custom Post Types</a></li>
      <li><a href="#menus">Menus</a></li>
			<li><a href="#theme-settings">Theme Settings</a></li>
      <li><a href="#media">Media</a></li>
      <li><a href="#seo">Search Engine Optimization (SEO)</a></li>
      <li><a href="#performance">Performance</a></li>
      <li><a href="#security">Security</a></li>
      <li><a href="#updates">Updates</a></li>
      <li><a href="#custom-css">Custom CSS</a></li>
      <li><a href="#troubleshooting">Troubleshooting</a></li>
		</ul>
	</nav>

  <section class="admin-section" id="welcome-to-wordpress">
		<h2>Welcome to WordPress</h2>
		<div class="wrap cf">

			<p>
        Welcome to your new WordPress website. WordPress is a "state-of-the-art semantic personal publishing platform with a focus on aesthetics, web standards, and usability. WordPress is both free and priceless at the same time." Your site is built using a custom WordPress theme from Eggbeater Creative. The following instructions will walk you through the basics of the WordPress Content Management System (CMS) and the custom features of your theme.
      </p>
      <p>
        When you first log in to WordPress you will be taken to the “Dashboard” page. This page is a customizable area that can be altered to suit your needs. To change the items that are displayed on this screen click on the “Screen Options” drop down at the top of the screen. From here you can select which boxes you would like to display on this screen. The screen options drop down appears in most areas within WordPress and can be used to customize certain elements such as the number of Posts that display on the screen at one time.
      </p>
      <p><img src="<?php echo $image_src; ?>dashboard.png" alt="img"></p>

		</div>
	</section>

  <section class="admin-section" id="users">
		<h2>Users</h2>
		<div class="wrap cf">

			<p>
        There are several user types within WordPress, each with its own set of attributes and abilities. These are Admin, Editor, Author, Contributor, Subscriber, Customer, Shop Manager and Agent. You can find detailed information on each of the standard WordPress roles <a href="https://codex.wordpress.org/Roles_and_Capabilities">here</a> and for the WooCommerce roles <a href="https://docs.woocommerce.com/document/roles-capabilities/">here</a>.
      </p>
      <p>
        In most instances your store owners and managers should have Admin access so that they can perform updates to content and plugins, as well as create new users. In some instances you may want to assign some employees the Editor role as this role gives them the ability to create content but does not allow them to perform plugin updates or alter user accounts.
      </p>
      <h3>How to add new Admin or Editor accounts</h3>
      <p>
        To add a New Admin or Editor go to the <a href="/wp-admin/users.php">users area here</a> and click on the “Add New” button at the top of the page. This will take you to a new screen where you can add the User’s details. Enter in the User’s details and select the User’s role from “Role” drop down. In the “Send User Notification” area you can either choose to have the user receive an email which will prompt them to setup their password by leaving the “Send the new user an email about their account” checkbox selected. If you choose to unclick this box, you will be prompted to enter a password for the User that you will then need to communicate to that person. Once you are done adding user details, click on the “Add New User” button at the bottom of the page.
      </p>
      <p><img src="<?php echo $image_src; ?>newuser.png" alt="img"></p>
      <h3>How to edit an Admin or Editor account</h3>
      <p>
        To edit an Admin or Editor account go to the <a href="/wp-admin/users.php">users area here</a> and either click on the “Administrator” or “Editor” links to narrow down the list of users or use the Search box on the right hand side to search for the user. Once you have found the user that you need to edit, click on their Username to enter into their account. Here you can edit the user’s details. Once you are done editing, click on the blue “Update User” button at the bottom of the page.
      </p>
      <h3>Deleting accounts</h3>
      <p>
        Before deleting a user, keep in mind that any content or data that the user has added will need to be associated with another User’s account, otherwise the content will be deleted as well.
      </p>
      <p>
        To delete an Admin or Editor account, go to the <a href="/wp-admin/users.php">users area here</a> and either click on the “Administrator” or “Editor” links to narrow down the list of users or use the Search box on the right hand side to search for the user. Once you have found the user you would like to remove click on the checkbox to the left of the Username. Once you have selected the User click on the "Bulk Actions” drop down and select “Delete” from the options. Once you click on “delete” you will be prompted by a screen asking “What should be done with content owned by this user?”. Here it is extremely important to choose the “Attribute all content to:” option and select one of your admin accounts from the dropdown list. Once you have selected a User click on the blue “Confirm Deletion” button.
      </p>
		</div>
	</section>

  <section class="admin-section" id="posts">
		<h2>Posts</h2>
		<div class="wrap cf">
			<p>Posts are the most basic element of a WordPress website. Posts show up on the Blog page of your site.</p>
      <h3>How to create a blog post</h3>
      <p>
        To create a blog post, go to the <a href="/wp-admin/edit.php">posts page</a> and click on the “Add New” button at the top of the page. Once there you will see a text area with preview text that says “Enter title here”. [screen cap of /wp-admin/post-new.php] Enter your title in this area. Below this you will see a "What you see is what you get (WYSIWYG)” editor. Enter your post content in this area. Below the WYSIWYG editor you will see an area for “Yoast SEO”. For more information on how this section works, see the “SEO” area below <a href="#seo">the “SEO” area below</a>.
      </p>
      <p>
        To the right of the Title and WYSIWYG areas you will see a box titled Publish, one called Categories, one for Tags and one for Featured Image.
      </p>
      <p>
        Featured Images are the images that are displayed in the preview of your blog post on the blog page of the your site. To add a Featured Image to your post click on the “Set featured image” link in the “Featured Image” box. Once you click this link, you will have the option of either choosing an image from the existing media or uploading a new image to the media area by selecting either the “Upload Files” or “Media Library” tab at the top of the window.
      </p>
      <p><img src="<?php echo $image_src; ?>upload.png" alt="img"></p>
      <p><img src="<?php echo $image_src; ?>media.png" alt="img"></p>
      <p>
        To add categories to your blog post, either click on the existing categories that apply to the post or click on the “+ Add New Category” link in the “Categories” area.
      </p>
      <p>
        To add Tags to your blog post, you can either add tags to the box in the Tags area or by clicking on the “Choose from the most used tags link”. *In general your theme does not use tags, however they are still a useful addition for your own sorting/searching purposes or for future features you may wish to add.
      </p>
      <p>
        Once you have added all of your content to your blog post, you can either choose to save your post as a draft by clicking on the “Save Draft" button in the “Publish” area, or you can choose to publish the post right away by clicking on the blue “Publish” button.
      </p>
      <p>
        If you would like to preview your post before you post it or after you have made any edits, you can do so by clicking the “Preview” button.
      </p>
      <p>
        Alternatively, instead of choosing to publish your post immediately, you can set a publish date once a draft has been saved by clicking on the “edit” link next to the “Publish” title in the “Publish” area. Here you will be able to select the date and time that you would like to publish your post on.
      </p>
      <p>
        You can also edit your post's visibility by clicking on the “Edit” button next to “Visibility” in the “Publish” area. Here you will be be able to choose from Public, Password Protected or Private. Public is the default setting, Password Protected will allow anyone with the post’s password to view the post. Private will only allow Admins and Editors to view the post.
      </p>
      <p>
        Once a post is published, the blue “Publish” link will change to a blue “Update” button. It is important that if you make any changes to your post after it is published that you click the “Update” button before exiting the page.
      </p>
      <h3>How to edit a blog post</h3>
      <p>
        To edit a blog post, go to <a href="/wp-admin/edit.php">the posts page</a> and click on the post that you would like to edit. If you don’t see the post that you are hoping to edit you can use the search box to the right side of the page to search for the post.
      </p>
      <p>
        Once a post has been published you can change the posts status and visibility in the “Publish” area. To change the post’s status, click on the “Edit” link next to the title “Status” in the “Publish” area. Here you will be given options for your post status in a dropdown.
      </p>
      <p>
        WordPress also utilizes a revisions area. Here you will be able to browse through all of the changes that you have made to your post and revert the post to one of the revisions if desired. To view revisions, click on the “Browse” link next to the “Revisions” title in the “Publish” area. After clicking the link you will be taken to the revisions page where you will see the current version of your post on the right hand side and the last revision on the left. Use the slide at the top of the screen to select previous revisions. Once you have found the revision that you would like to restore, click on the blue “Restore This Revision” button below the slider. To exit this this screen without making any changes click on the “Return to editor” link in the top left hand corner of the page.
      </p>
      <p><img src="<?php echo $image_src; ?>revision.png" alt="img"></p>
      <p>
        Once a post is published, the blue “Publish” link will change to a blue “Update” button. It is important that if you make any changes to your post after it is published that you click the “Update” button before exiting the page.
      </p>
      <h3>Notes on the WordPress WYSIWYG</h3>
      <p>
        The WordPress WYSIWYG is a very powerful tool that you will see throughout your website. It is the primary method for adding content to most posts and pages.  The WYSIWYG has two different modes that can be selected using the tabs at the top of the WYSIWIG area. These modes are visual and text. For the most part you will only need to use the Visual mode. The text mode is intended for entering in html content.
      </p>
      <p>
        Above the main WYSIWYG area you will see buttons for “Add Media” and “Add Form”. These buttons can be used to easily add images and forms to your page.
      </p>
      <p>
        Below this you will see a row of buttons that are used for formatting the content within the WYSIWYG. By highlighting the text within the WYSIWYG text area you can use these buttons to format that text. The first item in the row is a drop down menu that allows you to choose from Paragraph, H1 - H6 and preformatted text. Paragraph text is the standard body text across your site. H1 - H6 applies heading markup to that text. This is useful for adding subtitles on your page. In general it is best to avoid using the H1 tag as the main page title is already using this heading and adding more than one H1 to a page can hurt your SEO ranking. The next button “B” will make your text bolded. The next button “I” will make your text Italicized. The next button will make the selected text into a bulleted list. The next button over from this will make your text into a numbered list. The next button that looks like a quotation mark will turn the selected text into a blockquote. The next button over will align all of the selected text to the left, The next button will align all of the selected text to the centre. The next button will align all of the selected text to the right. The next icon over which looks like a chain link is the link icon. By highlighting text and clicking on this icon you can add a hyperlink to your text. A popup will be displayed where you can add the link that you would like to add. By typing in this box you can also search for pages/posts within your site. By clicking on the cog icon to the right of the text field in this popup you can set additional settings for this link including whether the link opens in a new window/tab. The next icon over that looks like a broken chain will remove the link associated with the highlighted text. The next icon over is a used to insert a read more tag. In general this icon is not applicable to your site as a seperate mechanism for creating excerpts is being used. Finally the last icon in the top row is used to expand or collapse additional options in the second row.
      </p>
      <p>
        The second row of items that can be used to alter your posts content begins with a dropdown titled "formats". This dropdown allows you to select preconfigured styling for your text such Text large or Small or Primary Color. These formats can be used to add styling to your text while still keeping it consistent with the rest of your theme. The next icon which is the letters ABC with a strike through it is used to put a strike through the selected text. The next icon which looks like a "-" is used to add a horizontal dividing line to your content. The next icon that looks like this "<u>A</u>" is used to change the text color. By clicking on the link you will be presented with a preconfigured color pallete to choose from or by clicking on the "Custom" link you can add a custom color to this area using the color wheel, RGB or hex value of the color. The next icon which looks like a clipboard with a T on it is used to "Paste as Text". This is useful if you are pasting text from Microsoft Word or a similar program. The next icon over that looks like an eraser is used to clear any formatting that is applied to text. The next icon over that looks like this "Ω" is used to insert a special character into the text. The next icon over is used to decrease the indent on the selected text. The icon over from that is used to increase the indentation of the selected text. The next two icons are used to undo or redo the changes that have been made. Finally the last icon with a "?" is used to display the keyboard shortcuts for the WordPress WYSIWYG.
      </p>
		</div>
	</section>

  <section class="admin-section" id="pages">
		<h2>Pages</h2>
		<div class="wrap cf">
			<p>Pages are the next basic element of WordPress websites. All of the methods for adding and editing posts apply to pages as well. The main difference between posts and pages is that pages do not have an archive (like your ‘Newsletter’ page does), or blog roll (like your ‘What’s New’ page) the way that posts do.</p>
      <p>
        To add or edit a page, go to the <a href="/wp-admin/admin.php?page=nestedpages">Pages area here</a>. On this page you will see a slightly different view than on the posts screen. This is the nested pages view. In this view, Pages with children have a triangle next to them to indicate that the view can be expanded or collapsed. Pages can be dragged and dropped into a specific order on this page or pages can be dropped into a child position below a parent page. As before, click on the page that you would like to edit to enter into the edit screen for that page.
      </p>
      <p><img src="<?php echo $image_src; ?>nested-pages.png" alt="img"></p>
          <p>
        As before, you can add a title, content, and adjust all of the page publish options. In addition, you can also specify the page’s parent from the “Page Attributes” area of this page.
      </p>
      <p><img src="<?php echo $image_src; ?>new-page.png" alt="img"></p>
      <p>
        "Custom Section" is as you would expect a custom section where you can add highly customizable content. The rest of these content types are layouts that bring in content from your custom post types. See below for more information on these custom post types.
      </p>
      <p>
        To remove a content layout from a page hover over the content section. When doing this you should see a Red circle with a minus sign in it. Click this button to remove the layout from the page.
      </p>
      <p>
        As before, remember to click the Blue “Publish/Update” button when you are done editing the page content.
      </p>
      <p>
        You can add content to each fx section by clicking on the black “Add Elements” button. These elements are WYSIWYG element, Title element, Image element, Video element, Document element and Button element. To add an element click on the element that you would like to add. Each element has its own set of custom fields where you can add your content.
      </p>
      <ul>
        <li>
          <b>WYSIWYG element</b> - Adds a WYSIWYG editor.
        </li>
        <li>
          <b>Title element</b> - Gives you an area to enter your title and a dropdown to select the Heading tag that will be applied to your title.
        </li>
        <li>
          <b>Image element</b> - Gives you an image upload button where you can upload an image
        </li>
        <li>
          <b>Video element</b> - Gives you a text area to add a title to your video and a text area to enter in an embed link from youtube or Vimeo e.g.  https://www.youtube.com/embed/9GoHMJoLQSw
        </li>
        <li>
          <b>Document element</b> - Allows you to easily add a downloadable document to your page. In this element you have a File upload area, a preview image upload, a text area for the Document Title, and a text area for additional Document Text.
        </li>
        <li>
          <b>Button element</b> - Allows you to easily add a button element to your page, e.g. you may want to add a callout button that will link to another page or website. In this element you have areas for Button Title, Button link and a dropdown selector for the Button style.
        </li>
      </ul>
      <p>
        Each element has an advanced options area.  To display the Element Advanced Options click the “Yes" button under the “Show Advanced Options” title for the element you would like to edit. You will now be presented with a number of options that can be customized.
      </p>
      <p><img src="<?php echo $image_src; ?>wysiwyg-advanced.png" alt="img"></p>
      <ul>
        <li>
          <b>Element custom class(es)</b> - Add classes to the element by entering them into the text area. These are useful if you want to apply custom CSS to the element. See Custom CSS section below.
        </li>
        <li>
          <b>Element custom options</b> - Select from a number of preconfigured custom options that control element width, text colour, background colour, and section visibility.
        </li>
      </ul>

		</div>
	</section>

  <section class="admin-section" id="custom-post-types">
		<h2>Custom Post Types</h2>
		<div class="wrap cf">
			<p>
        Custom Post Types are specially configured post types that allow you to enter in content with specific layouts and uses in mind.
      </p>
      <p>
        Each custom post type has its own content fields and categories that can be used for selective display of content on your site. The basic functionality of Custom Post Types is the same as for posts. To edit the content of a Custom Post Type or to add a new post click on the link in the left hand menu for the post type you would like to edit/add and either select the post you would like to edit or click on the blue “Add New” button.
      </p>
    </div>
	</section>

  <section class="admin-section" id="menus">
		<h2>Menus</h2>
		<div class="wrap cf">
			<p>
        Your site uses several menus for navigation. These menus are primary, mobile and shop. The primary menu is the menu which is display in the header of your page on desktop and some tablet computers. The mobile menu is used to create the navigation which is display in the header of your page on mobile and some tablet devices. The shop menu is the menu for the sidebar navigation that is displayed on the product pages. Please note that the sidebar on other pages is controlled by the parent/child/sibling relationship of  <a href="#pages">pages as described above</a>
      </p>
      <p>
        To edit one of these menus go to <a href="/wp-admin/nav-menus.php">this page in the WordPress admin</a>. At the top of the page you will see a dropdown menu with the title “Select a menu to edit”. Select the menu you would like to edit and click on the “Select” button. This will call up the menu so that you can begin editing. On the left hand side of the page you will see a list of links to pages, posts, categories etc. If you do not see the post type or category you would like to edit it may be hidden. To reveal other content types click on the “Screen Options” link at the top of the page. Click any of the post types, categories etc that you would like to display in the left hand select menu. Alternatively if there are any post types etc. that you would like to hide on this screen you can unclick those boxes so that they will not be shown.
      </p>
      <p>
        To add a page/post to a menu find that item in the left hand menu and click on the checkbox next to its name. Next click on the “Add to Menu” button. This will add the new menu item to the bottom of the area called “Menu structure” on the right hand side of the page. To move this item into the position that you would like it to appear hover the menu item until a four-sided arrow appears, click and drag it into the desired position. Move it into a position that is indented from the item above it will make this item a child menu item. Please note that only your mobile menu is setup to use child menu items.
      </p>
      <p>
        To add a custom link or link to content within a page or post click on the “custom links” area on the left hand side of the page and enter in the title and URL of the link you would like to add. To link to content within a page, you will need to know the “ID” of the item, this can be found by right clicking on the item in a web browser with developer tools turned on. Once you know the “ID” you can link to it by adding the link to the page it is on followed by the “ID” like this: /about/#some-content
      </p>
		</div>
	</section>

	<section class="admin-section" id="theme-settings">
		<h2>Theme Settings</h2>
		<div class="wrap cf">
			<h3>General Theme Settings </h3>
			<p>The <a href="/wp-admin/admin.php?page=theme-general-settings">Theme Setting area</a> is used to control some global settings for your theme. On this page there are three tabs where you can configure different parts of your site. These are General, Social Media and Contact Info.</p>
      <p>
        Under general settings you can update your site logo, and the copyrights text that is displayed at the bottom of your page footer.
      </p>
      <p>
        Under the “Social Media” tab you can add all of your social accounts. These are the links that appear in the header and footer of your site.
      </p>
      <p>
        Under the “Contact Info” tab this is where you can add all of your contact info including a google map. This the info that is pulled into the contact page, home page contact area and footer contact area.
      </p>
      <p><img src="<?php echo $image_src; ?>image-screen.jpg" alt="img"></p>

		</div>
	</section>

  <section class="admin-section" id="media">
		<h2>Media</h2>
		<div class="wrap cf">
			<p>
        The <a href="/wp-admin/upload.php">media area of your site</a> is where you can find all of the media files that have been uploaded to your site. By clicking on the “Add New” button at the top of this screen you can add multiple files at once to your site.
      </p>
      <p>
        By clicking on any of the images/files in the media area you will enter into that files admin page where a number of attributes about the file including its Title, Alt text, and Caption can be edited. In addition you will also be able to find an images permalink on this page.
      </p>
		</div>
	</section>

  <section class="admin-section" id="seo">
		<h2>Search Engine Optimization (SEO)</h2>
		<div class="wrap cf">
			<p>
        Your site has been setup with Google Analytics and Google Search Console and has the Yoast SEO plugin installed and configured. This plugin adds crucial information on your business, including which social accounts you own into the html header of the site. This information is used by Google and other search services to give authority to your businesses results within a search.
      </p>
      <p>
        To further configure your Yoast settings the easiest method is to run the  <a href="/wp-admin/admin.php?page=wpseo_configurator">Yoast settings wizard here</a>
      </p>
      <p>
        In addition to these site wide settings, each page and post on your site has a Yoast SEO area where keywords, SEO titles and SEO snippets can be added or edited. This area can be found on each page and post below the Content areas.
      </p>
      <p>
        The first area that you will want to add content to is the “Keyword(s)” section. These keywords are not added to your site's content and do not directly affect your SEO scores. These keywords are used simply to generate an SEO score that can be used as a guideline for crafting your content and SEO snippets. Keywords in this section should be either single words or related strings of words that directly relate to the content of the page you are editing. Eg you could use the term “About” however that would be a somewhat less than ideal keyword. A better keyword would be “About {name of business}”. You should not pack this area with keywords e.g. “about, wine, buy, u-brew” as this will not give you a useful result in your SEO score.
      </p>
      <p>
        The next step will be to edit your SEO title should you choose to do so. Editing this title can be helpful if you would like to provide a longer title that is more descriptive in your search results.
      </p>
      <p>
        Following this you can add or edit your SEO snippet. This is the brief description that will be displayed below your page title in the search results on Google or other search engines.
      </p>
      <p>
        This SEO snippet should be a clear, concise description of the content that is on this page. This description should be human readable. There is no benefit in packing this description with keywords as Google’s search algorithm is much smarter than we are and can tell when a snippet has been doctored for better search results. In these instances your sites authority will actually be downgraded due to this practice.
      </p>
      <p>
        Once you have added keywords and a snippet the Yoast SEO plugin will generate an SEO score for your page. This score is meant to be a guideline to help you craft your content and SEO snippet to best represent your business within search results. It is by no means the final word on whether your page will be displayed in search results. For example a page advertising “Air Jordan Sneakers” may have an excellent SEO score in Yoast however it may not show up in search results for “Air Jordan Sneakers” as this is a highly competitive search term whereas a page advertising “Hand tied fishing lures in Lac Le Jeune” may have a great search ranking even if it has a poor ranking in Yoast as this would be a search term with very few competitors. However it is a good idea to follow as many of the recommendations given by the plugin.
      </p>
      <p><img src="<?php echo $image_src; ?>yoast.png" alt="img"></p>
      <p>
        In addition to the SEO score your page will also be given a Readability score. Again this score can be used to help craft your page content so that it is both easy to read and understand for your customers and in term for the search bots that crawl your website. This score is intended to help you create human readable content.
      </p>
		</div>
	</section>

  <section class="admin-section" id="performance">
		<h2>Performance</h2>
		<div class="wrap cf">
			<p>
        Yours site’s performance has been optimized using the “W3 Total Cache” plugin. This plugin offers a number of levels of performance improvement including, caching, and minification. Since we have already configured these settings you shouldn’t need to alter anything here and in general it is best to have a web professional configure these settings. However should you choose to alter these settings they can be <a href="/wp-admin/admin.php?page=w3tc_general">found here</a>.
      </p>
      <p>
        In addition all of the css and javascript on your site has been minified in order to optimize performance.
      </p>
		</div>
	</section>

  <section class="admin-section" id="security">
		<h2>Security</h2>
		<div class="wrap cf">
			<p>
        Your site security has been configure using the Wordfence plugin. It has been configured to block brute force login attempts on your site, block malicious visitors by their IP addresses, require secure passwords and notify you when there are issues with your site. To change the frequency an type of notifications you receive from Wordfence you can do so  <a href="/wp-admin/admin.php?page=WordfenceSecOpt">here</a>.
      </p>
		</div>
	</section>

  <section class="admin-section" id="updates">
		<h2>Updates</h2>
		<div class="wrap cf">
			<p>
        From time to time WordPress and the plugins on your site will need to be updated. You can check to see what updates are available <a href="/wp-admin/update-core.php">here</a>. It is important that before performing any updates that a backup of your sites files and database are made. To perform a backup go <a href="/wp-admin/options-general.php?page=updraftplus">here</a> and click “Backup Now”.
      </p>
      <p>
        Once your site backup is complete return to the updates area and select any or all of the available plugins and WordPress updates and click the “Update” button.  After this is done it is best to click on the “Purge all Caches” link under the “Performance” menu at the top of the screen. Once this is done you should check your website to make sure that everything is looking as it should.
      </p>
		</div>
	</section>

  <section class="admin-section" id="custom-css">
		<h2>Custom CSS</h2>
		<div class="wrap cf">
			<p>
        CSS (Cascading Style Sheet) is what makes your site look the way that it does. WordPress has a custom CSS area should you need to make any minor aesthetic changes to your site. You can access this area <a href="/wp-admin/customize.php?return=%2Fwp-admin%2Findex.php">here</a>.
      </p>
		</div>
	</section>

  <section class="admin-section" id="troubleshooting">
		<h2>Troubleshooting</h2>
		<div class="wrap cf">
			<ul>
        <li>
          <a href="http://wordpress.tv/" target="_blank">WordPress TV</a> - <span>If your preferred method of learning new stuff is to watch a video, hit up WordPress.tv. It's packed with video tutorials from experienced WordPress users, covering practically every subject you might wish to get to grips with. And if it inspires you to share your own pearls of WordPress wisdom, you're welcome to submit your own videos for inclusion.</span>
        </li>
        <li>
          <a href="https://wplift.com/" target="_blank">WP Lift</a> - <span>Founded in 2010 and now reaching hundreds of thousands of WordPress users every year, WPLift is a comprehensive resource featuring loads of helpful tutorials and useful tips, as well as guides to getting the most out of themes and plugins, and much more besides – including some fantastic free themes.</span>
        </li>
        <li>
          <a href="http://wordpress.org/" target="_blank">WordPress.org</a> - <span>What better place to learn about WordPress than from its own site and the developers behind it. At WordPress.org you can not only download the software but learn the history behind the open source CMS, get access to popular themes and plugins and engage in active forums with other WordPress users.</span>
        </li>
        <li>
          <a href="http://www.wpbeginner.com/" target="_blank">WP Beginner</a> - <span>wpbeginner is a brilliant resource for anyone who's interested in WordPress but isn't quite sure where to start. Founded in July 2009 by Syed Balkhi, the main goal of this site is to provide quality tips, tricks, hacks and other WordPress resources that enable WordPress beginners to improve their sites.</span>
        </li>
        <li>
          <a href="https://docs.woocommerce.com" target="_blank">WooCommerce</a> - <span>Documentation, Reference Materials, and Tutorials for your WooCommerce products</span>
        </li>
        <li>
          <a href="mailto: support@eggbeater.ca" target="_blank">Contact Eggbeater</a> - <span>If all else fails send us an email with your question.</span>
        </li>
      </ul>
		</div>
	</section>

</section>
