jQuery(document).ready(function()
{
  jQuery(window).load(function()
  {
    //Target layout option
    var $doc_nav = jQuery('.admin-nav');

    //if exist
    if($doc_nav.length)
    {
      $doc_nav.find('a').click(function(e)
      {
        e.preventDefault();

        var $section = jQuery(jQuery(this).attr('href'));
        var $pos = $section.offset().top-65;

        jQuery('body,html').animate({
          scrollTop:$pos
        },800);
      });
    }


    //Sticky menu
    var $sticky = jQuery("#sticly-menu");

    if (!!$sticky.offset())
    {
      var $window = jQuery(window);
      var $stickyrStopper = jQuery('#wpfooter');
      var menu_width = $sticky.width()+2;

      var generalSidebarHeight = $sticky.innerHeight();
      var stickyTop = $sticky.offset().top;
      var stickOffset = 66;
      var stickyStopperPosition = $stickyrStopper.offset().top;
      var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
      var diff = stopPoint + stickOffset;

      $window.scroll(function()
      {
        var windowTop = $window.scrollTop(); // returns number

        if (stopPoint < windowTop)
        {
            $sticky.css({ position: 'absolute', top: diff });
            $sticky.removeClass('sticky');
        } else if (stickyTop < windowTop+stickOffset) {
            $sticky.css({ position: 'fixed', width:menu_width+'px', top: stickOffset });
            $sticky.addClass('sticky');
        } else {
            $sticky.css({position: 'absolute', top: 'initial'});
            $sticky.removeClass('sticky');
        }
      });
    }

  });
});
