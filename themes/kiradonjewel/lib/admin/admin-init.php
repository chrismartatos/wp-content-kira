<?php

/*-----------------------------------------------------------------------------------
    Documentation
-----------------------------------------------------------------------------------*/
function theme_doc()
{
	get_template_part("lib/admin/admin-documentation");
}

add_action('admin_menu', 'project_documentation');

function project_documentation()
{
	add_theme_page('Documentation', 'Documentation', 'edit_posts', 'theme-doc', 'theme_doc');
}

/*-----------------------------------------------------------------------------------
    CSS and JS
-----------------------------------------------------------------------------------*/
add_action('admin_enqueue_scripts', 'admin_documentation_style');

function admin_documentation_style()
{
  wp_enqueue_style('documentation-admin-styles', get_template_directory_uri().'/lib/admin/css/admin.css');
  wp_enqueue_script('documentation-admin-script', get_template_directory_uri().'/lib/admin/js/doc-script.js');
}
