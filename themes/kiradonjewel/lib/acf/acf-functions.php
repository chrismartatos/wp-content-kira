<?php


/*===========================================================================================================
    1. Register - Theme Options
============================================================================================================*/
if( function_exists('acf_add_options_page') )
{

	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false,
		'icon_url'    => 'dashicons-layout'
	));

/*
	acf_add_options_page(array(
		'page_title' 	=> 'Shop Settings',
		'menu_title' 	=> 'Shop Settings',
		'menu_slug' 	=> 'shop-option',
		'capability' 	=> 'edit_posts',
		'parent_slug' 	=> '',
		'icon_url' => 'dashicons-store',
		'position'		=> false,
		'redirect' 	=> false
	));



  ** Example to add child page settings under "theme-general-settings"
  /*
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Custom subpage',
		'menu_title' 	=> 'Subpage',
		'parent_slug' 	=> 'theme-general-settings',//Parent
	));
  */
}


/*===========================================================================================================
    2. Register - CSS & JS within Admin area
============================================================================================================*/
add_action('admin_enqueue_scripts', 'admin_style');

function admin_style()
{
  wp_enqueue_style('acf-admin-styles', get_template_directory_uri().'/lib/acf/css/acf-styles.css');
  wp_enqueue_script('acf-admin-script', get_template_directory_uri().'/lib/acf/js/acf-script.js');
}


/*===========================================================================================================
    5. Hide Custom Fields area from Admin on Staging and Production
============================================================================================================*/

/**
 * Hide ACF Admin on Staging/Production
 */

function awesome_acf_hide_acf_admin() {

    // get the current site url
    $site_url = get_bloginfo( 'url' );

    // an array of protected site urls
    $protected_urls = array(
    'https://www.kiradonjewel.com',
		'http://www.kiradonjewel.com',
    );

    // check if the current site url is in the protected urls array
    if ( in_array( $site_url, $protected_urls ) ) {

        // hide the acf menu item
        return false;

    } else {

        // show the acf menu item
        return true;

    }

}

add_filter('acf/settings/show_admin',__NAMESPACE__ . '\\awesome_acf_hide_acf_admin');
