<?php

/*-------------------------------------------------------------------------------------------------------
		Shop Gallery
--------------------------------------------------------------------------------------------------------*/
function acf_shop_gallery_shortcode($atts)
{
	$output = '';
  $thumb = '';
  $img = '';
  $image_html = '';
  $item_html = '';

    if( have_rows('shop_gallery_1','option') ):
      //Wrapper
    	$output .= '<div class="gallery gallery-columns-3 gallery-size-medium gallery-shortcode">';

        while( have_rows('shop_gallery_1','option') ) : the_row();

        $img = get_sub_field('image');

        if(!empty($img))
        {
          $image_html = '<img src="'.$img['url'].'" alt="'.get_the_title().' '.$img['alt'].'">';
          $item_html = (!empty(get_sub_field('link')))? '<a href="'.get_sub_field('link').'">'.$image_html.'</a>': '' ;

          $output .= '<figure class="gallery-item">';
          $output .= '<div class="gallery-icon portrait">'.$item_html.'</div>';
          $output .= '</figure>';
        }


        endwhile;

      $output .= '</div>';

    endif;

    return $output;
}

add_shortcode('shop_gallery', 'acf_shop_gallery_shortcode');
