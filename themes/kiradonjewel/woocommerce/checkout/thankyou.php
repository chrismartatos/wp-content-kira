<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;
?>

<div class="woocommerce-order">

	<?php
	if ( $order ) :

		do_action( 'woocommerce_before_thankyou', $order->get_id() );
		?>

		<?php if ( $order->has_status( 'failed' ) ) : ?>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php esc_html_e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
				<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php esc_html_e( 'Pay', 'woocommerce' ); ?></a>
				<?php if ( is_user_logged_in() ) : ?>
					<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php esc_html_e( 'My account', 'woocommerce' ); ?></a>
				<?php endif; ?>
			</p>

		<?php else : ?>

			<div class="spider top-spider">
        <div class="spiderweb"></div>
        <div class="spider-body">
          <div class="eye left"></div>
          <div class="eye right"></div>
        </div>
      </div>

			<?php
			$thank_you_image = get_field('thank_you_image','option');
			$user = wp_get_current_user();
			$roles = (!empty($user))? $user->roles : '';
			?>

			<div class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received">

				<?php if($roles=='wholesale_customer'): ?>
					<div class="thankyou-wholesale">
						<?= the_field('wholesale_thank_you','option'); ?>
					</div>
				<?php else: ?>
					<div class="thankyou-customer">
						<?= the_field('customer_thank_you','option'); ?>
					</div>
				<?php endif; ?>

				<div class="thankyou-notes">
					<?= the_field('thank_you_notes','option'); ?>
				</div>

				<?php if(!empty($thank_you_image)): ?>
	      <div class="thankyou-logo">
	        <img src="<?= $thank_you_image['url']; ?>" alt="may the jewel be with you" width="240">
	      </div>
				<?php endif; ?>
			</div>


			<ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">

				<li class="woocommerce-order-overview__order order">
					<?php esc_html_e( 'Order number:', 'woocommerce' ); ?>
					<strong><?php echo $order->get_order_number(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
				</li>

				<li class="woocommerce-order-overview__date date">
					<?php esc_html_e( 'Date:', 'woocommerce' ); ?>
					<strong><?php echo wc_format_datetime( $order->get_date_created() ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
				</li>

				<?php if ( is_user_logged_in() && $order->get_user_id() === get_current_user_id() && $order->get_billing_email() ) : ?>
					<li class="woocommerce-order-overview__email email">
						<?php esc_html_e( 'Email:', 'woocommerce' ); ?>
						<strong><?php echo $order->get_billing_email(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
					</li>
				<?php endif; ?>

				<li class="woocommerce-order-overview__total total">
					<?php esc_html_e( 'Total:', 'woocommerce' ); ?>
					<strong><?php echo $order->get_formatted_order_total(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
				</li>

				<?php if ( $order->get_payment_method_title() ) : ?>
					<li class="woocommerce-order-overview__payment-method method">
						<?php esc_html_e( 'Payment method:', 'woocommerce' ); ?>
						<strong><?php echo wp_kses_post( $order->get_payment_method_title() ); ?></strong>
					</li>
				<?php endif; ?>

			</ul>

		<?php endif; ?>

		<div class="wrap-checkout-info">
			<?php do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() ); ?>
			<?php do_action( 'woocommerce_thankyou', $order->get_id() ); ?>
		</div>

		<div class="the-end-wrap">

      <div class="spider bottom-spider">
        <div class="spiderweb"></div>
        <div class="spider-body">
          <div class="eye left"></div>
          <div class="eye right"></div>
        </div>
        <div class="legs left">
          <div class="leg"></div>
          <div class="leg"></div>
          <div class="leg"></div>
        </div>
        <div class="legs right">
          <div class="leg"></div>
          <div class="leg"></div>
          <div class="leg"></div>
        </div>
      </div>

      <h4>SPIDY WANTS TO THANK YOU <br>FOR YOUR ORDER</h4>
      <div class="msg"><p><a href="/products/jewels/all/" class="button white-button">BACK TO SHOP</a></p></div>

      <canvas id="starfield"></canvas>
    </div>

	<?php else : ?>

		<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'Thank you. Your order has been received.', 'woocommerce' ), null ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>

	<?php endif; ?>

</div>
