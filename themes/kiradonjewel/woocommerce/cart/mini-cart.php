<?php
/**
 * Mini-cart
 *
 * Contains the markup for the mini-cart, used by the cart widget.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/mini-cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.7.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<?php
if(is_cart() || is_checkout()):
	//silence
else: ?>

<?php do_action( 'woocommerce_before_mini_cart' ); ?>

<aside id="mini-cart" class="mini-cart">
  <?php
    get_template_part('woocommerce/cart/partials/notifications');

		get_template_part('woocommerce/cart/partials/loader');
  ?>

  <!-- ajax container -->
  <div id="ajax-mini-cart-data" class="clearfix<?php if ( WC()->cart->is_empty() ) { echo ' empty-cart-wrap'; } ?>" data-count="<?php echo WC()->cart->get_cart_contents_count(); ?>">
    <div class="mini-cart-content">
			<!--Stars-->
			<canvas id="starfield"></canvas>

      <?php
				get_template_part('woocommerce/cart/partials/spider');

			  get_template_part('woocommerce/cart/partials/close-btn');

        get_template_part('woocommerce/cart/partials/header');

        get_template_part('woocommerce/cart/partials/list');

        if ( ! WC()->cart->is_empty() ) :

          get_template_part('woocommerce/cart/partials/total');

          do_action( 'woocommerce_widget_shopping_cart_before_buttons' );

          get_template_part('woocommerce/cart/partials/nav-review');

        endif;

        do_action( 'woocommerce_after_mini_cart' );

      ?>
    </div><!-- .mini-cart-content -->
  </div><!-- #ajax-mini-cart-data -->

</aside>

<?php endif; ?>
