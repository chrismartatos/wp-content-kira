<?php use Roots\Sage\Extras; ?>

<ul class="cart_list product_list_widget" >

<?php if ( ! WC()->cart->is_empty() ) : ?>

  <form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">

    <?php do_action( 'woocommerce_before_cart_table' ); ?>
    <?php do_action( 'woocommerce_before_cart_contents' ); ?>
    <?php do_action( 'woocommerce_before_mini_cart_contents' ); ?>

    <?php
      foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {

        $product_data = Extras\get_product_id($cart_item_key, $cart_item);
        $_product     = $product_data['product'];
        $_product_id  = $product_data['product_id'];

        if ($_product) {

          $product_values    = Extras\build_product_values($_product, $cart_item, $cart_item_key);
          $product_name      = $product_values['product_name'];
          $thumbnail         = $product_values['thumbnail'];
          $product_price     = $product_values['product_price'];
          $product_permalink = $product_values['product_permalink'];

          include(locate_template('woocommerce/cart/partials/item.php'));
        }
      }

      do_action( 'woocommerce_mini_cart_contents' );

    ?>
    <input type="hidden" name="update_cart" value="1">
    <?php do_action( 'woocommerce_cart_actions' ); ?>
    <?php wp_nonce_field( 'woocommerce-cart' ); ?>
  </form>

<?php

  else :

    get_template_part('woocommerce/cart/partials/empty-item');

endif;

?>

</ul>
