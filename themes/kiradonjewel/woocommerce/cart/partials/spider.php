<?php $empty = ( WC()->cart->is_empty() )? 'empty-cart': 'no-empty';?>

<div class="moon <?= $empty; ?>">
  <ul>
    <li></li>
    <li></li>
    <li></li>
    <li></li>
    <li></li>
    <li></li>
  </ul>
</div>

<!--div class="cloud-container <?= $empty; ?>">
  <div class="cloud"></div>
  <div class="cloud"></div>
  <div class="cloud"></div>
  <div class="cloud"></div>
  <div class="cloud"></div>
</div-->

<div class="spider <?= $empty; ?>">
  <div class="spiderweb"></div>
  <div class="spider-body">
    <div class="eye left"></div>
    <div class="eye right"></div>
  </div>
  <div class="legs left">
    <div class="leg"></div>
    <div class="leg"></div>
    <div class="leg"></div>
  </div>
  <div class="legs right">
    <div class="leg"></div>
    <div class="leg"></div>
    <div class="leg"></div>
  </div>
</div>
