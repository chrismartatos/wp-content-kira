<?php
$user = wp_get_current_user();
?>

<div class="offer-msg msg-top">
  <?php
  if ( in_array( 'wholesale_customer', (array) $user->roles ) ):
    the_field('wholesale_shop_cart_notification','options');
  else:
    the_field('shop_cart_notification','options');
  endif;
  ?>
</div>

<?php if ( ! WC()->cart->is_empty() ) : ?>
<div class="offer-msg msg-bottom">
  <?php
  if ( in_array( 'wholesale_customer', (array) $user->roles ) ):
    the_field('wholesale_shop_cart_notification_products','options');
  else:
    the_field('shop_cart_notification_products','options');
  endif;
  ?>
</div>
<?php endif; ?>
