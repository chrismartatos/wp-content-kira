<?php
use Roots\Sage\Extras;
?>

<!-- CART LOADER -->
<div id="cart-loader" class="">
  <span class="wrap-loader">
    <?php echo Extras\include_svg('kiradonjewel-logo-diamond'); ?>
    <div class="loading-text">Loading cart...</div>
  </span>
</div>
