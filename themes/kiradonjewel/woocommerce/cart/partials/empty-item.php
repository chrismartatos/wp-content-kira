
<?php
$shop_page_url = get_term_link( 16, 'product_cat' );
?>
<!-- Empty Message -->
<li class="empty">
  <div class="wrap-msg">
    <span class="empty-msg first-item">It’s quiet in here...</span>
    <span class="empty-msg second-item">Start adding items to your cart!</span>
  </div>

  <nav class="cart-shop-now">
    <a href="<?= $shop_page_url; ?>" class="btn button white-button">SHOP NOW</a>
  </nav>
</li>
<?php get_template_part('woocommerce/cart/partials/acf-bottom'); ?>
