<div class="total">
  <span class="label"><?php _e( 'Total', 'woocommerce' ); ?>:</span>
  <span id="get-price" class="price"><?php echo WC()->cart->get_cart_subtotal(); ?></span>
</div>
