<li class="<?php echo esc_attr( apply_filters( 'woocommerce_mini_cart_item_class', 'mini_cart_item', $cart_item, $cart_item_key ) ); ?>">
  <div class="cart-item-wrap">
    <div class="left-col">
      <a href="<?php echo get_permalink( $_product_id ); ?>">
      <?php echo apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key ); ?>
      </a>
    </div>

    <div class="right-col">
      <h5 class="title">
      <?php
      echo $_product->get_name();
      ?>
      </h5>

      <div class="wrap-price-qty clearfix">
        <div class="wrap-price">
          <span class="custom-label">Price:</span>
          <?php echo apply_filters( 'woocommerce_widget_cart_item_quantity', '<span class="quantity">' . sprintf( '%s &times; %s', $cart_item['quantity'], $product_price ) . '</span>', $cart_item, $cart_item_key ); ?>
        </div>
      </div>

      <?php
      echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
        '<a href="%s" class="remove-item mini-cart-link-styles fa fa-remove" aria-label="%s" data-key="'.$cart_item_key.'" data-product_id="%s" data-product_sku="%s"></a>',
        esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
        __( 'Remove this item', 'woocommerce' ),
        esc_attr( $_product_id ),
        esc_attr( $_product->get_sku() )
      ), $cart_item_key );
      ?>
    </div><!--.right-col-->
  </div><!--.wrap-->

</li>
