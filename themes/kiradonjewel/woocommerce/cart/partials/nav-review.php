<nav class="cart-review-order">
  <a href="<?php echo esc_url( wc_get_cart_url() );?>" class="cart-button btn button white-button">
    <?php esc_html_e( 'View Cart', 'woocommerce' ); ?>
  </a>
  <a href="<?php echo esc_url( wc_get_checkout_url() );?>" class="checkout-button btn button white-button">
    <?php esc_html_e( 'Checkout', 'woocommerce' ); ?>
  </a>
</nav>
<?php get_template_part('woocommerce/cart/partials/acf-bottom'); ?>
