<?php global $wp_query; ?>

<div class="container">
  <?php if (!have_posts()) : ?>
    <div class="alert alert-warning">
      <?php _e('Sorry, no results were found.', 'sage'); ?>
    </div>
    <?php get_search_form(); ?>
  <?php endif; ?>

  <?php
  if (isset($_GET['post_type'])) {
    if($_GET['post_type']==='product'):
      $string = ($wp_query->post_count>1)?'s':'';
      if (have_posts()){
        echo '<p>Keyword: <strong>'.$_GET['s'].'</strong> | Found: <strong>'.$wp_query->post_count.'</strong> product'.$string.'</p><hr><br /><br />';
      }
  ?>
    <script>
    document.getElementsByTagName("BODY")[0].classList.add('woocommerce');;
    </script>
    <ul class="products">
    <?php
      while (have_posts()) : the_post();
      wc_get_template_part('content', 'product');
      endwhile;
    ?>
    </ul>
    <hr />
  <?php
  endif;
  }
  else
  {
    while (have_posts()) : the_post();
    get_template_part('templates/content', 'search');
    endwhile;
  }
  ?>

  <?php the_posts_navigation(); ?>
</div>
