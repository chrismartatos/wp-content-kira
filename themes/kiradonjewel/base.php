<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;


?>

<!doctype html>
<html <?php language_attributes(); ?>>

  <?php get_template_part('templates/head'); ?>

  <body <?php body_class(); ?>>

    <?php
    //PRELOADER
    get_template_part('templates/preloader');
    ?>

    <?php
    //CUSTOMER NOTIFICATIONS
    //get_template_part('templates/shop-elements/header-notifications');
    ?>

    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->

    <?php
      do_action('get_header');
      get_template_part('templates/header');
    ?>

		<?php
    //PAGE HEADER
    get_template_part('templates/page', 'header');

    if(is_woocommerce()): get_template_part('templates/shop-elements/shop-page-header'); endif; ?>

    <div class="main-content" role="document">
      <main class="main">
        <div class="page-content">
          <div class="page-wrapper clearfix">
            <?php include Wrapper\template_path(); ?>
          </div>
        </div>
      </main><!-- /.main -->
    </div><!-- /.wrap -->


    <div class="site-container-cart">
      <?php get_template_part('woocommerce/cart/mini-cart'); ?>
    </div>

    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();

      //ACF Embed scripts
      get_template_part('templates/base-embed-scripts');
    ?>
  </body>
</html>
