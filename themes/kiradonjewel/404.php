<?php use Roots\Sage\Titles; ?>

<h1 class="container">404 - Page Not Found</h1>

<div class="container">
  <div class="alert alert-warning">
    <?php _e('Sorry, but the page you were trying to view does not exist. <br />Please use the main menu or send us an email.', 'sage'); ?>
  </div>

  <?php get_search_form(); ?>
</div>
