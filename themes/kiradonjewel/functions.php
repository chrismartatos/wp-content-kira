<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */


$sage_includes = [
  'lib/assets.php',     // Scripts and stylesheets
  'lib/extras.php',     // Custom functions
  'lib/setup.php',      // Theme setup
  'lib/titles.php',     // Page titles
  'lib/wrapper.php',    // Theme wrapper class
  'lib/customizer.php', // Theme customizer
  'lib/acf/acf-functions.php', // ACF init
  'lib/admin/admin-init.php', // Documentation
  'lib/cpt/lookbook.php',
  'lib/shortcodes.php',
  'lib/woocommerce/wc-setup.php',
  'lib/woocommerce/wc-checkout.php',
  'lib/woocommerce/wc-tabs.php',
  'lib/woocommerce/wc-custom-cart.php',
  'lib/woocommerce/wc-products.php',
];

foreach ($sage_includes as $file)
{
  if (!$filepath = locate_template($file))
  {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);


/*-------------------------------------------------------------------------------------
    ACF NAV MENU
-------------------------------------------------------------------------------------*/
add_filter('wp_nav_menu_objects', 'my_wp_nav_menu_objects', 10, 2);

function my_wp_nav_menu_objects( $items, $args ) {

// loop
foreach( $items as &$item ) {

  // vars
  $acf_menu_thumb = get_field('thumbnail', $item);
  $html_thumb = (!empty($acf_menu_thumb))? '<div class="image-circle" style="background-image:url('.$acf_menu_thumb['url'].');"></div>': '';

  // append icon
  if( !empty($acf_menu_thumb) ) {
    $item->classes[] = 'circle-thumb-added';
    $item->title .= $html_thumb;
  }
}

// return
return $items;

}


/*-------------------------------------------------------------------------------------
    Enque Javascript - Isotope, history
-------------------------------------------------------------------------------------*/
add_action( 'wp_enqueue_scripts', 'register_extras_scripts' );

function register_extras_scripts()
{
  if(!is_checkout())
  {
    wp_enqueue_script( 'history' , get_template_directory_uri() . '/assets/extras/history.js', array( 'jquery' ), null, true );
    wp_enqueue_script( 'relax' , get_template_directory_uri() . '/assets/extras/rellax.min.js', array( 'jquery' ), null, true );
    wp_enqueue_script( 'stars' , get_template_directory_uri() . '/assets/extras/stars.js', array( 'jquery' ), null, true );
  }
}


/*-------------------------------------------------------------------------------------
      Body Classes - options
-------------------------------------------------------------------------------------*/
add_filter('body_class', 'generate_body_classes');

function generate_body_classes($classes)
{
  //defaults
  $fx_header_active;
  $fx_content_active;
  $fx_header_global_active;

  //Flexible Content - Page Header
  $fx_header_active = ( have_rows('fx_page_header') ) ? 'fx-page-header-active':'fx-page-header-empty';

  //Flexible Content - Content
  $fx_content_active = ( have_rows('fx_add_content') ) ? 'fx-content-active':'fx-content-empty';

  //shop header
  if(is_woocommerce() || is_shop())
  {
    $get_term = get_queried_object();

    $fx_header_global_active = ( have_rows('cloned_page_header', $get_term) ) ? 'fx-page-header-global-active':'fx-page-header-global-empty';
  }

  //Generate classes
  $classes[] = $fx_header_global_active;
  $classes[] = $fx_header_active;
  $classes[] = $fx_content_active;

  return $classes;
}

/*-----------------------------------------------------------------------------------
    First time login for notary
-----------------------------------------------------------------------------------*/
add_action( 'wp_login', 'track_user_logins', 10, 2 );

function track_user_logins( $user_login, $user )
{
    if( $login_amount = get_user_meta( $user->id, 'login_amount', true ) ){
        // They've Logged In Before, increment existing total by 1
        update_user_meta( $user->id, 'login_amount', ++$login_amount );
    } else {
        // First Login, set it to 1
        update_user_meta( $user->id, 'login_amount', 1 );
    }
}

add_shortcode( 'login_content', 'login_content' );

function login_content( $atts )
{
    if( is_user_logged_in() )
    {
      $user_info = wp_get_current_user();
      $user_ID   = (int) $user_info->data->ID;
      $user_meta=get_userdata($user_ID);
      $user_roles=$user_meta->roles;

        // Get current total amount of logins (should be at least 1)
        $login_amount = get_user_meta( get_current_user_id(), 'login_amount', true );
        $welcome = '<div class="entry-content nota-message">'.get_field('welcome_message', 'options').'</div>';

        if(in_array('notary', $user_roles))
        {
          // return content based on how many times they've logged in.
          if( $login_amount == 1 ){
              return $welcome;
          } else if( $login_amount == 2 ){
              return $welcome;
          } else if( $login_amount == 3 ){
              return $welcome;
          } else {
              return "";
          }
        }
    }
}
